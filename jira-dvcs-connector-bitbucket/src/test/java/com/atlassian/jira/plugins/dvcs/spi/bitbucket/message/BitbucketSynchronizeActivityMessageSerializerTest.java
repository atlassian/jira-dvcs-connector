package com.atlassian.jira.plugins.dvcs.spi.bitbucket.message;

import com.atlassian.jira.plugins.dvcs.model.Message;
import com.atlassian.jira.util.json.JSONObject;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anySet;
import static org.mockito.Matchers.eq;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.assertFalse;


public class BitbucketSynchronizeActivityMessageSerializerTest {
    private static final int PAGE_NUM = 1;
    private static final int VERSION = 1;
    private static final String PAGE_URL = "Page Url";

    @Mock
    private BitbucketSynchronizeActivityMessage payload;

    @InjectMocks @Spy
    private BitbucketSynchronizeActivityMessageSerializer testedClass;

    private JSONObject json;

    @Mock
    private Message message;


    @BeforeMethod
    private void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testPageUrlSerialized() throws Exception {
        json = new JSONObject();
        when(payload.getPageUrl()).thenReturn(PAGE_URL);

        testedClass.serializeInternal(json, payload);

        assertTrue(json.has("pageUrl"));
        assertFalse(json.has("pageNum"));
        assertEquals(json.getString("pageUrl"), PAGE_URL);
    }

    @Test
    public void testDeserializeInternalWithPageUrl() throws Exception {
        json = new JSONObject();
        json.put("pageUrl", PAGE_URL);

        testedClass.deserializeInternal(json, VERSION);
        verify(testedClass).createActivityMessage(eq(PAGE_URL), anySet(), anySet(), any());
    }

    @Test @Deprecated
    public void testDeserializeInternalWithPageNumber() throws Exception {
        json = new JSONObject();
        json.put("pageNum", PAGE_NUM);

        testedClass.deserializeInternal(json, VERSION);
        verify(testedClass).createActivityMessage(eq(PAGE_NUM), anySet(), anySet(), any());
    }
}