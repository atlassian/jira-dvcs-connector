package com.atlassian.jira.plugins.dvcs.spi.bitbucket;

import org.apache.commons.lang.StringUtils;
import org.scribe.model.Token;

public class BitbucketOAuthAuthentication {
    private final String accessToken;

    public BitbucketOAuthAuthentication(String accessToken) {
        this.accessToken = accessToken;
    }

    public static String generateAccessTokenString(Token accessTokenObj) {
        return accessTokenObj.getToken() + "&" + accessTokenObj.getSecret();
    }

    public static Token generateAccessTokenObject(String accessTokenStr) {
        if (!StringUtils.isBlank(accessTokenStr)) {
            String[] parts = accessTokenStr.split("&");
            if (parts.length == 2) {
                return new Token(parts[0], parts[1]);
            }
        }
        return null;
    }

    public String getAccessToken() {
        return accessToken;
    }
}
