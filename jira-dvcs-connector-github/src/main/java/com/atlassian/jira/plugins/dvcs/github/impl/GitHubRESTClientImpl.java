package com.atlassian.jira.plugins.dvcs.github.impl;

import com.atlassian.jira.plugins.dvcs.github.api.GitHubRESTClient;
import com.atlassian.jira.plugins.dvcs.github.api.model.GitHubRateLimit;
import com.atlassian.jira.plugins.dvcs.github.api.model.GitHubRepositoryHook;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.ClientFilter;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.util.List;

/**
 * An implementation of {@link GitHubRESTClient}.
 *
 * @author Stanislav Dvorscak
 */
@Component
@ParametersAreNonnullByDefault
public class GitHubRESTClientImpl extends AbstractGitHubRESTClientImpl implements GitHubRESTClient {

    private static HTTPBasicAuthFilter getBasicAuthFilter(final String username, final String password) {
        return new HTTPBasicAuthFilter(username, password);
    }

    @Override
    @Nonnull
    public GitHubRepositoryHook addHook(final Repository repository, final GitHubRepositoryHook hook) {
        final WebResource webResource = resource(repository, "/hooks");
        return webResource.type(MediaType.APPLICATION_JSON_TYPE).post(GitHubRepositoryHook.class, hook);
    }

    @Override
    public void deleteHook(final Repository repository, final GitHubRepositoryHook hook) {
        final WebResource webResource = resource(repository, "/hooks/" + hook.getId());
        webResource.delete();
    }

    @Override
    @Nonnull
    public List<GitHubRepositoryHook> getHooks(final Repository repository) {
        final WebResource hooksWebResource = resource(repository, "/hooks");
        return getAll(hooksWebResource, GitHubRepositoryHook[].class);
    }

    @Override
    @Nonnull
    public List<GitHubRepositoryHook> getHooks(final Repository repository, final String username, final String password) {
        final WebResource hooksWebResource = resource(repository, "/hooks");
        final ClientFilter basicAuthFilter = getBasicAuthFilter(username, password);
        return getAll(hooksWebResource, GitHubRepositoryHook[].class, basicAuthFilter);
    }

    @Nonnull
    @Override
    public GitHubRateLimit getRateLimit(final String accountName, final String password) {
        final WebResource resource = getClient().resource("https://api.github.com").path("rate_limit");
        // Not authenticating would return the different, lower limit for unauthenticated requests
        resource.addFilter(getBasicAuthFilter(accountName, password));
        final MultivaluedMap<String, String> responseHeaders = resource.get(ClientResponse.class).getHeaders();
        return GitHubRateLimit.fromHeaders(responseHeaders);
    }
}
