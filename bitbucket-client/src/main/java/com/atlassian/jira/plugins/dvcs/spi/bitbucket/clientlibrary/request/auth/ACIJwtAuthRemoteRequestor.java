package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.auth;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fusion.aci.api.event.UninstallationCompletedEvent;
import com.atlassian.fusion.aci.api.model.ConnectApplication;
import com.atlassian.fusion.aci.api.service.ACIJwtService;
import com.atlassian.fusion.aci.api.service.ACIRegistrationService;
import com.atlassian.fusion.aci.api.service.exception.InvalidSharedSecretException;
import com.atlassian.fusion.aci.api.service.exception.UninstalledException;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.ApiProvider;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.BaseRemoteRequestor;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.BitbucketRequestException;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.HttpClientProvider;
import com.google.common.annotations.VisibleForTesting;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpRequestBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Nonnull;

import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * Uses a {@link ACIJwtService} to generate a JWT token for JWT-based authentication with a remote.
 */
public class ACIJwtAuthRemoteRequestor extends BaseRemoteRequestor {

    private final Logger log = LoggerFactory.getLogger(ACIJwtAuthRemoteRequestor.class);

    private final String principalId;
    private final ACIRegistrationService registrationService;
    private final ACIJwtService jwtService;
    private final EventPublisher eventPublisher;

    public ACIJwtAuthRemoteRequestor(
            @Nonnull final ApiProvider apiProvider,
            @Nonnull final String principalId,
            @Nonnull final ACIRegistrationService registrationService,
            @Nonnull final ACIJwtService jwtService,
            @Nonnull final HttpClientProvider httpClientProvider,
            @Nonnull final EventPublisher eventPublisher) {
        super(checkNotNull(apiProvider), checkNotNull(httpClientProvider));

        checkArgument(isNotBlank(principalId));
        this.principalId = principalId;
        this.registrationService = checkNotNull(registrationService);
        this.jwtService = checkNotNull(jwtService);
        this.eventPublisher = checkNotNull(eventPublisher);
    }

    @Override
    protected void onConnectionCreated(HttpClient client, HttpRequestBase request, Map<String, ?> params)
            throws IOException {
        try {
            String jwtToken = jwtService.generateJwtToken(
                    BITBUCKET_CONNECTOR_APPLICATION_ID, principalId, request.getMethod(), request.getURI().toURL());
            request.setHeader("Authorization", "JWT " + jwtToken);
        } catch (UninstalledException e) {
            // We might have missed the event informing us that this installation was uninstalled and that
            // we should carry out the necessary clean up. Throw the event again.
            // It is safe to assume that the connect application can be retrieved here because we found an installation
            // that was uninstalled
            final ConnectApplication connectApplication = registrationService.get(BITBUCKET_CONNECTOR_APPLICATION_ID);
            eventPublisher.publish(new UninstallationReminderEvent(principalId, connectApplication));
        } catch (InvalidSharedSecretException e) {
            log.debug("Not sending the request and throwing unauthorized exception since the shared secret is invalid",
                principalId);
            throw new BitbucketRequestException.Unauthorized_401();
        } catch (Exception e) {
            final String message = format("Unable to generate a JWT token for principal %s", principalId);
            throw new IllegalStateException(message, e);
        }
    }

    /**
     * We implement our own {@link UninstallationCompletedEvent} so that we can re-fire the event if we missed it
     * the first time.
     */
    @VisibleForTesting
    static class UninstallationReminderEvent implements UninstallationCompletedEvent {
        private final String principalId;
        private final ConnectApplication connectApplication;

        public UninstallationReminderEvent(
                @Nonnull final String principalId,
                @Nonnull final ConnectApplication connectApplication) {
            this.principalId = checkNotNull(principalId);
            this.connectApplication = checkNotNull(connectApplication);
        }

        @Nonnull
        @Override
        public String getPrincipalId() {
            return principalId;
        }

        @Nonnull
        @Override
        public ConnectApplication getConnectApplication() {
            return connectApplication;
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            final UninstallationReminderEvent that = (UninstallationReminderEvent) o;
            return Objects.equals(principalId, that.getPrincipalId()) &&
                    Objects.equals(connectApplication, that.getConnectApplication());
        }

        @Override
        public int hashCode() {
            return Objects.hash(principalId, connectApplication);
        }

        @Override
        public String toString() {
            return String.format("principalUuid: %s, connectApplication: %s", principalId, connectApplication);
        }
    }
}
