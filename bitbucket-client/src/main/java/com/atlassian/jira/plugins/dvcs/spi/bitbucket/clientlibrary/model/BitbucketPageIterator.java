package com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model;

import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.RemoteRequestor;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.request.RemoteResponse;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nonnull;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static com.google.common.base.Preconditions.checkNotNull;

public abstract class BitbucketPageIterator<T> implements Iterator<T>, Iterable<T> {
    /**
     * The default request limit.
     */
    public static final int REQUEST_LIMIT = 30;
    // services
    private final RemoteRequestor requestor;
    private final String urlIncludingApi;
    // configs
    private BitbucketPullRequestPage<T> currentPage;

    protected BitbucketPageIterator(@Nonnull final RemoteRequestor requestor, @Nonnull final String urlIncludingApi) {
        this(requestor, urlIncludingApi, REQUEST_LIMIT);
    }

    protected BitbucketPageIterator(
            @Nonnull final RemoteRequestor requestor,
            @Nonnull final String urlIncludingApi,
            final int requestLimit) {
        this.requestor = checkNotNull(requestor);
        this.urlIncludingApi = urlIncludingApi + "?pagelen=" + requestLimit;
    }

    @Override
    public boolean hasNext() {
        // not initialized
        if (currentPage == null) {
            readPage();
        }

        return !currentPage.getValues().isEmpty() || currentPage.getNext() != null;
    }

    private void readPage() {
        String url = currentPage == null ? urlIncludingApi : currentPage.getNext();
        // maybe we're just at the end
        if (StringUtils.isBlank(url)) {
            return;
        }

        currentPage = requestor.get(url, null, this::transformFromJson);
    }

    protected abstract BitbucketPullRequestPage<T> transformFromJson(RemoteResponse response);

    @Override
    public T next() {
        if (currentPage.getValues().isEmpty()) {
            readPage();
        }

        if (currentPage.getValues().isEmpty()) {
            throw new NoSuchElementException();
        }

        T currentItem = currentPage.getValues().remove(0);

        // possibly read the next page
        if (currentPage.getValues().isEmpty()) {
            readPage();
        }

        return currentItem;
    }

    @Override
    public Iterator<T> iterator() {
        return this;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Remove is unsupported.");
    }
}
