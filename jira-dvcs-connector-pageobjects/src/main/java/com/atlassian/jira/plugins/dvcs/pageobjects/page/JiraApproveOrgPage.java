package com.atlassian.jira.plugins.dvcs.pageobjects.page;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;

/**
 * The JIRA Approval page that is displayed when linking JIRA and Bitbucket to approve the connection
 * from JIRA side.
 */
public class JiraApproveOrgPage implements Page {
    public static final String JIRA_APPROVAL_PAGE_URL = "/secure/BitbucketPostInstallApprovalAction.jspa";

    @Inject
    private PageBinder pageBinder;

    @Inject
    private PageElementFinder elementFinder;

    @ElementBy(id = "approve-button")
    private PageElement approveButton;

    @Override
    public String getUrl() {
        return JIRA_APPROVAL_PAGE_URL;
    }

    @WaitUntil
    public void waitUntilLoaded() {
        waitUntilTrue("The 'Approve Org' page did not load", approveButton.timed().isVisible());
    }

    public void approve() {
        approveButton.click();
    }
}
