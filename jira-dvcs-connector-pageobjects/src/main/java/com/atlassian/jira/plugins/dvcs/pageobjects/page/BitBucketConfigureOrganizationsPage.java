package com.atlassian.jira.plugins.dvcs.pageobjects.page;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

import static com.atlassian.jira.plugins.dvcs.pageobjects.util.AciPageObjectUtils.addBitbucketAsDvcsType;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Represents the page to link repositories to projects.
 */
public class BitBucketConfigureOrganizationsPage extends BaseConfigureOrganizationsPage {
    @ElementBy(id = "oauthClientId")
    PageElement oauthKeyInput;

    @ElementBy(id = "oauthSecret")
    PageElement oauthSecretInput;

    @ElementBy(id = "atlassian-token")
    PageElement atlassianTokenMeta;

    @ElementBy(id = "oauthBbClientId")
    private PageElement oauthBbClientId;

    @ElementBy(id = "oauthBbSecret")
    private PageElement oauthBbSecret;

    @Override
    public BitBucketConfigureOrganizationsPage addOrganizationSuccessfully(final String organizationAccount,
                                                                           final OAuthCredentials oAuthCredentials, final boolean autoSync, final String username, final String password) {
        return addOrganizationSuccessfully(organizationAccount, oAuthCredentials, autoSync, username, password, true);
    }

    public BitBucketConfigureOrganizationsPage addOrganizationSuccessfully(final String organizationAccount,
                                                                           final OAuthCredentials oAuthCredentials, final boolean autoSync, final String username, final String password,
                                                                           final boolean doOAuthDance) {
        getLinkBitbucketAccountButton().click();
        waitFormBecomeVisible();

        if (isAciEnabled()) {
            addBitbucketAsDvcsType(dvcsTypeSelect);
        }
        selectDvcsType("bitbucket");

        if (!autoSync) {
            autoLinkNewRepos.click();
        }

        fillInDetailsAndSubmit(organizationAccount, oAuthCredentials);

        if (doOAuthDance) {
            if (isFormOpen().by(5, SECONDS)) {
                // if form still open, assume it hits the weird clear text error, where some or all fields are cleared after filled in
                //  just retry the filling and submit again.
                fillInDetailsAndSubmit(organizationAccount, oAuthCredentials);
            }

            waitUntilFalse(atlassianTokenMeta.timed().isPresent());
            pageBinder.bind(BitbucketGrandOAuthAccessPage.class).grantAccess();
        }

        waitUntilTrue(getLinkBitbucketAccountButton().timed().isPresent());

        if (autoSync) {
            JiraPageUtils.checkSyncProcessSuccess(pageBinder);
        }

        return this;
    }

    // used by devstatus it.com.atlassian.jira.plugin.devstatus.webdriver.setup.SetupBitbucketCtk
    public BitBucketConfigureOrganizationsPage addCtkServerOrganizationSuccessfully(final String organizationAccount,
                                                                                    final OAuthCredentials oAuthCredentials, final boolean autoSync, final String username, final String password) {
        return addOrganizationSuccessfully(organizationAccount, oAuthCredentials, autoSync, username, password, false);
    }

    private void fillInDetailsAndSubmit(final String organizationAccount, final OAuthCredentials oAuthCredentials) {
        organization.clear().type(organizationAccount);

        oauthBbClientId.clear().type(oAuthCredentials.key);
        oauthBbSecret.clear().type(oAuthCredentials.secret);

        addOrgButton.click();
    }
}
