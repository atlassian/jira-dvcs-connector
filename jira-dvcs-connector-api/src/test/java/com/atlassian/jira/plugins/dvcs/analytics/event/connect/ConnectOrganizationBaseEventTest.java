package com.atlassian.jira.plugins.dvcs.analytics.event.connect;

import com.atlassian.jira.plugins.dvcs.model.Organization;
import org.mockito.Mock;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import javax.annotation.Nonnull;

import static com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory.create2LOCredential;
import static com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory.createPrincipalCredential;
import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ConnectOrganizationBaseEventTest {

    @Mock
    private Organization org;

    @BeforeTest
    public void setup() {
        initMocks(this);

        when(org.getId()).thenReturn(123);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void constructor_throwsNullPointer_whenNullProvided() {
        new TestEvent(null);
    }

    @Test
    public void constructor_createsEvent_whenPrincipalNoBracketsProvided() {
        runConstructPrincipalTest("principal", "principal");
    }

    @Test
    public void constructor_createsEvent_whenPrincipalWithBracketsProvided() {
        runConstructPrincipalTest("{principal}", "principal");
    }

    @Test
    public void constructor_createsEvent_whenPrincipalWithOnlyBracketsProvided() {
        runConstructPrincipalTest("{}", "");
    }

    private void runConstructPrincipalTest(String principal, String expectedPrincipal) {
        when(org.getCredential()).thenReturn(createPrincipalCredential(principal));

        ConnectOrganizationBaseEvent event = new TestEvent(org);

        assertThat(event.getPrincipalId()).isEqualTo(expectedPrincipal);
        assertThat(event.getOrganizationId()).isEqualTo(org.getId());
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void constructor_throwsIllegalArgument_oauthCredential() {
        when(org.getCredential()).thenReturn(create2LOCredential("key", "secret"));
        new TestEvent(org);
    }

    private static class TestEvent extends ConnectOrganizationBaseEvent {
        protected TestEvent(@Nonnull final Organization org) {
            super(org);
        }
    }
}