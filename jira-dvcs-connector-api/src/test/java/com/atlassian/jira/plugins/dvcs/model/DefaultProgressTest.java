package com.atlassian.jira.plugins.dvcs.model;


import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

import static org.apache.commons.lang3.SerializationUtils.deserialize;
import static org.apache.commons.lang3.SerializationUtils.serialize;
import static org.fest.assertions.api.Assertions.assertThat;

public class DefaultProgressTest {
    private final ExecutorService executor = Executors.newSingleThreadExecutor();

    @AfterClass
    protected void destroy() throws Exception {
        executor.shutdownNow();
    }

    @Test
    public void instancesShouldBeSerializableForUseWithinCacheKeys() {
        // Set up
        final int jiraCount = 42;
        final DefaultProgress original = new DefaultProgress();
        original.setJiraCount(jiraCount);

        // Invoke
        final DefaultProgress roundTripped = deserialize(serialize(original));

        // Check
        assertThat(roundTripped.getJiraCount()).isEqualTo(jiraCount);
    }

    @Test
    public void waitForFinish_shouldExit_whenFinished() throws Exception {
        final DefaultProgress progress = new DefaultProgress();

        concurrentMarkAsFinished(progress, 300);

        progress.waitForFinish();

        assertThat(progress.isFinished()).isTrue();
    }

    @Test
    public void waitForFinish_shouldExitImmediately_whenAlreadyFinished() throws Exception {
        final DefaultProgress progress = new DefaultProgress();

        progress.finish();

        progress.waitForFinish();

        assertThat(progress.isFinished()).isTrue();
    }

    @Test(expectedExceptions = TimeoutException.class)
    public void waitForFinish_shouldThrowException_whenTimedOut() throws Exception {
        final DefaultProgress progress = new DefaultProgress();

        progress.waitForFinish(100);
    }

    @Test
    public void waitForFinish_shouldExitOnFinished_whenProgressCompletesBeforeTimeout() throws Exception {
        final DefaultProgress progress = new DefaultProgress();

        concurrentMarkAsFinished(progress, 300);

        progress.waitForFinish(700);

        assertThat(progress.isFinished()).isTrue();
    }

    private void concurrentMarkAsFinished(final DefaultProgress progress, final int millis) {
        executor.submit((Runnable) () -> {
            try {
                Thread.sleep(millis);
            } catch (InterruptedException e) {
            }
            progress.finish();
        });
    }
}
