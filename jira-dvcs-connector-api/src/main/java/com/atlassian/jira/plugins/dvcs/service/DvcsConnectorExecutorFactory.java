package com.atlassian.jira.plugins.dvcs.service;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * Service to abstract away constructor of our Executors, mainly exists to support testing
 */
public interface DvcsConnectorExecutorFactory {

    /**
     * Create the thread pool executor for the {@link com.atlassian.jira.plugins.dvcs.service.LinkerService}
     *
     * @return A thread pool executor configured for the LinkerService
     */
    ThreadPoolExecutor createLinkerServiceThreadPoolExecutor();

    /**
     * Create a thread pool executor for performing webhook cleanup.
     *
     * @return a thread pool executor for performing webhook cleanup.
     */
    ThreadPoolExecutor createWebhookCleanupThreadPoolExecutor();

    /**
     * Create a thread pool executor for performing repository deletion.
     *
     * @return a new thread pool executor for performing repository deletion.
     */
    ThreadPoolExecutor createRepositoryDeletionThreadPoolExecutor();

    /**
     * Drain the work queue of the supplied executor and wait for it to shutdown
     *
     * @param owningClassName The class which wants to shutdown the executor, used in the logs
     * @param executor        The executor to shutdown
     */
    void shutdownExecutor(String owningClassName, ThreadPoolExecutor executor);
}
