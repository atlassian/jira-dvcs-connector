package com.atlassian.jira.plugins.dvcs.model.credential;

import java.util.Optional;

public class OAuthCredentialVisitor extends AbstractOptionalCredentialVisitor<OAuthCredential> {
    private static final OAuthCredentialVisitor visitor = new OAuthCredentialVisitor();

    public static OAuthCredentialVisitor visitor() {
        return visitor;
    }

    public static boolean isAOAuthCredential(Credential credential) {
        return credential.accept(visitor).isPresent();
    }

    @Override
    public Optional<OAuthCredential> visit(final TwoLeggedOAuthCredential credential) {
        return Optional.of(credential);
    }

    @Override
    public Optional<OAuthCredential> visit(final ThreeLeggedOAuthCredential credential) {
        return Optional.of(credential);
    }
}
