package com.atlassian.jira.plugins.dvcs.analytics.event.lifecycle;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.jira.plugins.dvcs.analytics.AnalyticsService;
import com.atlassian.jira.plugins.dvcs.analytics.AnalyticsService.OrganizationApprovalLocation;

import javax.annotation.ParametersAreNonnullByDefault;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Event fired when an organization fails to transition from PENDING to APPROVED
 */
@ParametersAreNonnullByDefault
@EventName("jira.dvcsconnector.organization.approval.failed")
public class OrganizationApprovalFailedEvent {

    private final int organizationId;
    private final OrganizationApprovalLocation organizationApprovalLocation;
    private final AnalyticsService.OrganizationApprovalFailedReason reason;

    public OrganizationApprovalFailedEvent(final int organizationId,
                                           final OrganizationApprovalLocation organizationApprovalLocation,
                                           final AnalyticsService.OrganizationApprovalFailedReason reason) {
        this.organizationId = organizationId;
        this.organizationApprovalLocation = checkNotNull(organizationApprovalLocation,
                "Location where an Organization was approved must be specified");
        this.reason = checkNotNull(reason,
                "Reason why an Organization failed approval must be specified");
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public OrganizationApprovalLocation getOrganizationApprovalLocation() {
        return organizationApprovalLocation;
    }

    public AnalyticsService.OrganizationApprovalFailedReason getReason() {
        return reason;
    }
}
