package com.atlassian.jira.plugins.dvcs.auth.impl;

import com.atlassian.jira.plugins.dvcs.auth.Authentication;
import com.atlassian.jira.plugins.dvcs.auth.AuthenticationFactory;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.model.credential.Credential;
import com.atlassian.jira.plugins.dvcs.model.credential.ThreeLeggedOAuthCredential;

import javax.inject.Named;

@Named
public class DefaultAuthenticationFactory implements AuthenticationFactory {
    @Override
    public Authentication getAuthentication(Repository repository) {
        return getAuthentication(repository.getCredential());
    }

    @Override
    public Authentication getAuthentication(Organization organization) {
        return getAuthentication(organization.getCredential());
    }

    private Authentication getAuthentication(final Credential credential) {
        return credential.accept(ThreeLeggedOAuthCredential.visitor())
                .<Authentication>map(threeLeggedCredential -> new OAuthAuthentication(threeLeggedCredential.getAccessToken()))
                .orElseGet(() -> Authentication.ANONYMOUS);
    }
}
