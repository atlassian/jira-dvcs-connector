package com.atlassian.jira.plugins.dvcs.dao;

/**
 * Callbacks for workings in streams.
 *
 * @param <T>
 * @author Stanislav Dvorscak
 */
public interface StreamCallback<T> {

    /**
     * @param e
     */
    void callback(T e);

}
