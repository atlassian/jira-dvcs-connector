package com.atlassian.jira.plugins.dvcs.model;

import javax.annotation.Nonnull;

/**
 * Represents the id number associated with a Message
 */
public class MessageId {

    private final int id;

    public MessageId(final int id) {
        this.id = id;
    }

    public MessageId(@Nonnull final Integer id) {
        this.id = id;
    }

    public MessageId(final Message message) {
        this.id = message.getId();
    }

    public int getId() {
        return id;
    }
}
