package com.atlassian.jira.plugins.dvcs.model;

import com.google.common.collect.ImmutableList;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@ParametersAreNonnullByDefault
public final class ChangesetBuilder {
    int id;
    Date synchronizedAt;
    int repositoryId;
    String node;
    String rawAuthor;
    String author;
    Date date;
    String rawNode;
    String branch;
    String message;
    List<String> parents;
    String authorEmail;
    Boolean smartcommitAvaliable;
    List<ChangesetFile> files;
    int allFileCount;
    Integer version;

    /**
     * This builder is supposed to be created from Changeset static factory method
     */
    ChangesetBuilder() {
    }

    public ChangesetBuilder withId(final int val) {
        id = val;
        return this;
    }

    public ChangesetBuilder withNode(final String val) {
        node = val;
        return this;
    }

    public ChangesetBuilder withRawAuthor(final String val) {
        rawAuthor = val;
        return this;
    }

    public ChangesetBuilder withAuthor(final String val) {
        author = val;
        return this;
    }

    public ChangesetBuilder withDate(final Date val) {
        date = val;
        return this;
    }

    public ChangesetBuilder withRawNode(final String val) {
        rawNode = val;
        return this;
    }

    public ChangesetBuilder withBranch(final String val) {
        branch = val;
        return this;
    }

    public ChangesetBuilder withMessage(final String val) {
        message = val;
        return this;
    }

    @Nullable
    public ChangesetBuilder withParents(@Nullable final List<String> val) {
        parents = val == null ? null : ImmutableList.copyOf(val);
        return this;
    }

    public ChangesetBuilder withAuthorEmail(final String val) {
        authorEmail = val;
        return this;
    }

    public ChangesetBuilder withSmartcommitAvaliable(final Boolean val) {
        smartcommitAvaliable = val;
        return this;
    }

    @Nullable
    public ChangesetBuilder withFiles(@Nullable final Collection<ChangesetFile> val) {
        files = val == null ? null : ImmutableList.copyOf(val);
        return this;
    }

    public ChangesetBuilder withAllFileCount(final int val) {
        allFileCount = val;
        return this;
    }

    public ChangesetBuilder withVersion(final Integer val) {
        version = val;
        return this;
    }

    public Changeset build() {
        return new Changeset(this);
    }
}
