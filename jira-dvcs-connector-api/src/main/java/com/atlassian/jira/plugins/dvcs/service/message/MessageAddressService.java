package com.atlassian.jira.plugins.dvcs.service.message;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Service for retrieving the message address given a payload type and route ID
 */
@ParametersAreNonnullByDefault
public interface MessageAddressService {

    /**
     * Creates message address, necessary by publishing and routing.
     *
     * @param payloadType type of payload
     * @param id          of route
     * @return created message address
     */
    <P extends HasProgress> MessageAddress<P> get(Class<P> payloadType, String id);
}
