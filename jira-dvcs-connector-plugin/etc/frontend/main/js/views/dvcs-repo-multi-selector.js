/**
 * represents the aui multi selector, to add (link) repos
 */
define('jira-dvcs-connector/bitbucket/views/dvcs-repo-multi-selector', [
    'jira-dvcs-connector/lib/backbone',
    "underscore"
], function (
    Backbone,
    _
) {
    "use strict";

    return Backbone.View.extend({
        addButtonSelector: '.addDvcsRepoButton',
        select2Selector: 'select',

        events: {
            "click .addDvcsRepoButton": '_addRepo'
        },

        initialize: function () {
            this.pendingRepos = {};
            this._bindDomElements();
        },

        render: function() {
            if (this.collection.length) {
                this.collection.filterDisabled().forEach(this._addRepoToSelect2.bind(this));
            }
        },

        _bindDomElements: function () {
            this.collection.on('add', this._repoStateChanged, this);
            this.collection.on('remove', this._repoDeleted, this);
            this.collection.on('change:linked', this._repoStateChanged, this);

            this.addButton = this.$el.find(this.addButtonSelector);

            this.select2 = this.$el.find(this.select2Selector).auiSelect2();
            this.select2.on('change', this._onSelect2Change.bind(this));

            this.addButton.prop('disabled', true);
        },

        _addRepo: function () {
            var self = this;
            var selected = this.select2.val();
            if (selected && selected.length) {
                selected.map(function(repoId) {
                   return self.collection.get(repoId);
                }).filter(function(repo) {
                    return !!repo;
                }).map(function(repo) {
                    repo.enable();
                    return self._addPendingAddRepo(repo);
                });
                this.addButton.prop('disabled', true);
            }
        },

        _onSelect2Change: function (e) {
            var selected = this.select2.val();
            var enabled = selected && selected.length;
            this.addButton.prop('disabled', !enabled);
        },

        _addRepoToSelect2: function(repo) {
            this.select2.append(dvcs.connector.plugin.soy.dvcs.accounts.select2Option({repo: repo.toJSON()}));
            this.select2.val(null).trigger("change");
        },

        _repoDeleted: function(repo) {
            if (!repo.isEnabled()) {
                this._removeRepoFromSelect2(repo);
            }
        },

        _removeRepoFromSelect2: function(repo) {
            this.select2.find("option[value=" + repo.id+ "]").remove();
            this.select2.val(null).trigger("change");
            this._removePendingAddRepo(repo);
        },

        _repoStateChanged: function(repo, repoEnabled, options) {
            if (repo.isEnabled()) {
                this._removeRepoFromSelect2(repo);
            }else {
                this._addRepoToSelect2(repo);
            }
        },

        _addPendingAddRepo: function(repo) {
            this.pendingRepos = this.pendingRepos || {};
            this.pendingRepos[repo.id] = repo;
            return repo;
        },

        _removePendingAddRepo: function(repo) {
            if(repo.isEnabled() && this.pendingRepos && this.pendingRepos[repo.id]){
                this.recentlyAddedRepos = this.recentlyAddedRepos || [];
                this.recentlyAddedRepos.push(repo);
                delete this.pendingRepos[repo.id];

                // check if no more pending repos
                if (!_.keys(this.pendingRepos).length &&
                    this.recentlyAddedRepos && this.recentlyAddedRepos.length) {
                    this.trigger('jira-dvcs-connector:repos-batch-added', this.recentlyAddedRepos);
                    this.recentlyAddedRepos = [];
                }
            }
        },

        spin: function() {
            this.disabled = true;
            this.select2.prop("disabled", true);
            this.$el.find('.select2-search-field').addClass('select-spinner');
        },

        spinStop: function() {
            this.select2.prop("disabled", false);
            this.$el.find('.select2-search-field').removeClass('select-spinner');
            this.disabled = false;
        }
    });
});
