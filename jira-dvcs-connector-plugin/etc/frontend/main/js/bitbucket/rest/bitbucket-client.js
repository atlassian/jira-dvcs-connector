/**
 * An API client for working with Bitbucket Cloud
 *
 * @see https://confluence.atlassian.com/bitbucket/use-the-bitbucket-cloud-rest-apis-222724129.html
 *
 * @module jira-dvcs-connector/bitbucket-client
 */
define('jira-dvcs-connector/bitbucket/rest/bitbucket-client', [], function () {

    var BITBUCKET_URL = "https://bitbucket.org";

    /**
     * Create a new client with the given optional Bitbucket URL.
     *
     * If no URL is provided, will use the default public Bitbucket URL.
     *
     * @param {string} [bitbucketUrl] The bitbucket URL to use for the client
     * @constructor
     */
    return function Client(bitbucketUrl) {

        if (!bitbucketUrl) {
            bitbucketUrl = BITBUCKET_URL;
        }

        /**
         * The URL this client instance will use to contact Bitbucket
         */
        this.bitbucketUrl = bitbucketUrl;

        /**
         * API for working with Connect addons.
         */
        this.addons = {

            /**
             * Generate a URL that can be used to redirect to the authorize addon page in Bitbucket.
             *
             * The provided URIs should be passed un-encoded.
             *
             * @param descriptorUri The URL for the addon descriptor to be installed
             * @param redirectUri The URL to redirect to after authorization
             */
            generateAddonAuthorizeRedirect: function (descriptorUri, redirectUri) {

                var authorizeUrl = bitbucketUrl + '/site/addons/authorize';
                return authorizeUrl +
                    '?descriptor_uri=' + encodeURIComponent(descriptorUri) +
                    '&redirect_uri=' + encodeURIComponent(redirectUri);
            }
        };
    };

});