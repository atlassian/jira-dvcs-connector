/**
 * Wrapper for DVCS Connector REST endpoints.
 *
 * @module jira-dvcs-connector/rest/dvcs-connector-rest-client
 */
define('jira-dvcs-connector/rest/dvcs-connector-rest-client', [], function () {
    'use strict';

    var $ = require('jquery');
    var Navigate = require('jira-dvcs-connector/util/navigate');

    /**
     * REST client for working with Organizations
     *
     * @param {string} baseURL The JIRA base URL to use for the client
     * @constructor
     */
    function OrganizationClient(baseURL, atlToken) {
        /**
         * @returns {string} The JIRA base URL for this client
         */
        this.getBaseUrl = function () {
            return baseURL;
        }

        /**
         * @returns {string} The atlToken that this client was initialised with, if any
         */
        this.getAtlToken = function () {
            return atlToken;
        }

        /**
         * @returns {string} A query param with the atl_token value set or empty string depending if atlToken was
         * supplied in the constructor
         */
        this.getAtlTokenQueryParam = function () {
            return atlToken ? '?atl_token=' + encodeURIComponent(atlToken) : ''
        }
    }

    /**
     * Where the approval occurred, direct reflection of the Java implementation in OrganizationApprovalLocation
     * @readonly
     * @enum {string}
     */
    OrganizationClient.prototype.ApprovalLocationEnum = {
        DURING_INSTALLATION_FLOW: 'DURING_INSTALLATION_FLOW',
        ON_ADMIN_SCREEN: 'ON_ADMIN_SCREEN',
        UNKNOWN: 'UNKNOWN'
    },

    /**
     * Approve the scopes for the provided organisation
     *
     * @param {Integer} orgId The ID of the organisation to approve
     * @param {ApprovalLocationEnum} approvalLocation The part of the UI that issued the approval
     * @returns {jqXHR} The JQuery XHR object representing the promise
     */
        OrganizationClient.prototype.approve = function (orgId, approvalLocation) {
            var queryParams = {
                approvalLocation: approvalLocation
            };
            if (this.getAtlToken()) {
                queryParams.atlToken = this.getAtlToken();
            }
            return $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                url: this.getBaseUrl() + '/rest/bitbucket/1.0/organization/' + encodeURIComponent(orgId)
                + '/approve' + Navigate.buildQueryParams(queryParams)
            });
        };

    /**
     * Delete the organization with the specified id
     * @param {integer} orgId the ID of the organization to delete
     * @returns {jqXHR} The JQuery XHR object representing the promise
     */
    OrganizationClient.prototype.remove = function (orgId) {
        return $.ajax({
            type: 'DELETE',
            url: this.getBaseUrl() + '/rest/bitbucket/1.0/organization/' + encodeURIComponent(orgId)
            + this.getAtlTokenQueryParam()
        });
    }

    /**
     * Refresh the organisation repository list
     *
     * @param {integer} orgId the ID of the organisation to refresh
     * @returns {jqXHR} the JQuery XHR object representing the request
     */
    OrganizationClient.prototype.refreshRepositoryList = function (orgId) {
        return $.ajax({
            type: 'GET',
            url: this.getBaseUrl() + '/rest/bitbucket/1.0/organization/' + orgId + '/syncRepoList'
        });
    };

    /**
     * Set whether or not new repositories are auto-linked for the provided organization
     *
     * @param {int} orgId The ID of the organization to set
     * @param {boolean} autolink whether to autolink or not
     * @returns {jqXHR} the JQuery XHR object representing the request
     */
    OrganizationClient.prototype.autoLinkRepositories = function (orgId, autolink) {
        return $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: this.getBaseUrl() + '/rest/bitbucket/1.0/org/' + orgId + '/autolink',
            data: JSON.stringify({
                payload: autolink
            })
        });
    };

    /**
     * Set whether or not smart commits are enabled for new repositories for the provided organization
     *
     * @param {int} orgId The ID of the organization to set
     * @param {boolean} enabled whether to auto-enable smart commits or not
     * @returns {jqXHR} the JQuery XHR object representing the request
     */
    OrganizationClient.prototype.autoEnableSmartCommits = function (orgId, enabled) {
        return $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: this.getBaseUrl() + '/rest/bitbucket/1.0/org/' + orgId + '/globalsmarts',
            data: JSON.stringify({
                payload: enabled
            })
        });
    };

    /**
     * Update the 'automatically enable X' settings for the given organization.
     *
     * Note that this is the recommended way to update these settings as a unit, rather than calling each
     * of the individual update methods as it avoids race conditions and treats the update as an atomic operation.
     * It is also more efficient than calling each operation individually.
     *
     * @param {int} orgId The ID of the organization to update
     * @param {boolean} enableAutolink whether to auto-link new repositories
     * @param {boolean} enableSmarcommits whether to auto-enable smart commits on new repositories
     * @returns {*}
     */
    OrganizationClient.prototype.updateAutoSettings = function (orgId, enableAutolink, enableSmarcommits) {
        return $.ajax({
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            url: this.getBaseUrl() + '/rest/bitbucket/1.0/organization/' + orgId + '/autosettings',
            data: JSON.stringify({
                enableAutolink: enableAutolink,
                enableSmartCommits: enableSmarcommits
            })
        });
    };


    /**
     * retrieve all repositories in given organization
     *
     * @param orgId {int} orgId The ID of the organization to get it repositories
     * @returns {*}
     */
    OrganizationClient.prototype.getRepositories = function (orgId) {
        return $.ajax({
            type: 'GET',
            url: this.getBaseUrl() + '/rest/bitbucket/1.0/repository/find?orgId=' + encodeURIComponent(orgId)
        });
    };

    /**
     * REST client for working with the internal Users API
     *
     * @param {string} baseURL The base URL of the JIRA instance the API is for
     *
     * @constructor
     */
    function InternalUserClient(baseURL) {
        /**
         * @returns {string} The JIRA base URL for this client
         */
        this.getBaseUrl = function () {
            return baseURL;
        }
    }

    /**
     * Retrieve the value of the 'has seen feature discovery' flag for the current user
     *
     * @returns {jqXHR} the jQuery XHR object representing the request
     */
    InternalUserClient.prototype.getHasUserSeenFeatureDiscovery = function() {
        console.info("Getting flag!");
        return $.ajax({
            type: 'GET',
            url: this.getBaseUrl() + '/rest/dvcs-connector-internal/1.0/user/flag/featureDiscoverySeen',
            dataType: 'json'
        })
    };

    /**
     * Update the value of the 'has seen feature discovery' flag for the current user
     *
     * @param {boolean} hasSeenFeatureDiscovery The value of the flag to set
     *
     * @returns {jqXHR} the jQuery XHR object representing the request
     */
    InternalUserClient.prototype.setHasUserSeenFeatureDiscovery = function(hasSeenFeatureDiscovery) {
        return $.ajax({
            type: 'PUT',
            url: this.getBaseUrl() + '/rest/dvcs-connector-internal/1.0/user/flag/featureDiscoverySeen',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({
                value: hasSeenFeatureDiscovery
            })
        })
    };

    /**
     * A client for interacting with the DVCS Connector REST API
     *
     * @param {string} baseURL The JIRA base URL to use for the client (e.g. http://my.domain.com/jira)
     * @constructor
     */
    function RestClient(baseURL, atlToken) {

        /**
         * The base URL for the JIRA instance this client works with
         */
        this.getBaseUrl = function () {
            return baseURL;
        };

        /**
         * API for working with Organizations
         *
         * @type {OrganizationClient}
         */
        this.organization = new OrganizationClient(baseURL, atlToken);

        /**
         * Internal APIs
         *
         * @type {object}
         */
        this.internal = {

            /**
             * Internal API for working with users
             */
            user: new InternalUserClient(baseURL)
        }
    }

    return RestClient;
});