var fs = require('fs');
var path = require('path');

var commonConf = require('fusion-js-test-common').commonConfiguration;

module.exports = function (config) {

    var projectBaseDir = __dirname + '../../../';
    var paths = {
        jsSource: path.join(projectBaseDir, 'target/classes/js'),
        testSource: path.join(projectBaseDir, 'etc/frontend/test/src')
    };

    config.set({
        src: {
            main: {
                dir: paths.jsSource,
                files: [
                    {pattern: paths.jsSource + '/lib/aui/*.js', included: false},
                    {pattern: paths.jsSource + '/bitbucket/**/*.js', included: false},
                    {pattern: paths.jsSource + '/rest/*.js', included: false},
                    {pattern: paths.jsSource + '/ui/*.js', included: false},
                    {pattern: paths.jsSource + '/util/*.js', included: false},
                    {pattern: paths.jsSource + '/views/*.js', included: false},
                    {pattern: paths.jsSource + '/models/*.js', included: false},
                    {pattern: paths.jsSource + '/admin/*.js', included: false},
                    {pattern: paths.jsSource + '/admin/view/*.js', included: false},
                    {pattern: paths.jsSource + '/analytics/*.js', included: false},
                    {pattern: paths.jsSource + '/feature-discovery/*.js', included: false}
                ]
            },
            test: {
                dir: paths.testSource,
                files: [
                    {pattern: paths.testSource + '/**/*-test.js', included: false}
                ]
            }
        },
        reporters: ['progress', 'junit'],
        junitReporter: {
            outputFile: path.join(projectBaseDir, '/target/surefire-reports/karma-results.xml'),
            suite: ''
        },
        port: 9876,
        colors: true,
        logLevel: config.LOG_INFO,
        autoWatch: true,
        browsers: ['PhantomJS'],
        captureTimeout: 60000,
        singleRun: false
    });

    commonConf(config);
};