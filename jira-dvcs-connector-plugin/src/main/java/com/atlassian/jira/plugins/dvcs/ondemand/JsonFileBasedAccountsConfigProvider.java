package com.atlassian.jira.plugins.dvcs.ondemand;

import com.atlassian.jira.config.Feature;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.JiraProperties;
import com.atlassian.jira.plugins.dvcs.util.ExceptionLogger;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import static com.atlassian.jira.config.CoreFeatures.ON_DEMAND;
import static com.google.common.base.Preconditions.checkNotNull;

@ExportAsService(AccountsConfigProvider.class)
@Component
public class JsonFileBasedAccountsConfigProvider implements AccountsConfigProvider {
    /**
     * The name of the dark feature that enables the "integrated accounts"
     * feature, which FYI is also enabled in OnDemand mode and in Dev mode.
     * <p>
     * This flag allows us to turn on the feature during tests without enabling dev mode.
     */
    public static final Feature INTEGRATED_ACCOUNTS_FEATURE =
            () -> AccountsConfigProvider.class.getName() + ".integratedAccounts";

    /**
     * @see #absoluteConfigFilePath
     */
    public static final String ENV_ONDEMAND_CONFIGURATION = "ondemand.properties";

    /**
     * Default value of {@link #ENV_ONDEMAND_CONFIGURATION}
     */
    public static final String ENV_ONDEMAND_CONFIGURATION_DEFAULT = "/data/jirastudio/home/ondemand.properties";

    private static final Logger log = ExceptionLogger.getLogger(JsonFileBasedAccountsConfigProvider.class);

    private final String absoluteConfigFilePath =
            System.getProperty(ENV_ONDEMAND_CONFIGURATION, ENV_ONDEMAND_CONFIGURATION_DEFAULT);

    private final FeatureManager featureManager;
    private final JiraProperties jiraProperties;

    @Autowired
    public JsonFileBasedAccountsConfigProvider(
            @ComponentImport final FeatureManager featureManager,
            @ComponentImport final JiraProperties jiraProperties) {
        this.featureManager = checkNotNull(featureManager);
        this.jiraProperties = checkNotNull(jiraProperties);
    }

    @Override
    @Nullable
    public AccountsConfig provideConfiguration() {
        try {
            final File configFile = getConfigFile();
            if (!configFile.exists() || !configFile.canRead()) {
                throw new IllegalStateException(absoluteConfigFilePath + " file can not be read.");
            }

            final GsonBuilder builder = new GsonBuilder();
            builder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES);
            final Gson gson = builder.create();

            return gson.fromJson(new InputStreamReader(new FileInputStream(configFile)), AccountsConfig.class);
        } catch (JsonParseException json) {
            log.error("Failed to parse config file " + absoluteConfigFilePath, json);
            return null;
        } catch (Exception e) {
            log.info("File not found, probably not ondemand instance or integrated account should be deleted.", e);
            return null;
        }
    }

    File getConfigFile() {
        return new File(absoluteConfigFilePath);
    }

    @Override
    public boolean supportsIntegratedAccounts() {
        final boolean isDevMode = jiraProperties.isDevMode();
        final boolean isFeatureFlagSet = featureManager.isEnabled(INTEGRATED_ACCOUNTS_FEATURE);
        final boolean isOnDemand = featureManager.isEnabled(ON_DEMAND);
        log.debug("OD = {}, dev = {}, flag = {}", new Object[]{isOnDemand, isDevMode, isFeatureFlagSet});
        return isOnDemand | isDevMode | isFeatureFlagSet;
    }

}
