package com.atlassian.jira.plugins.dvcs.scheduler;

import com.atlassian.fusion.aci.api.model.ConnectApplication;
import com.atlassian.fusion.aci.api.model.RemoteApplication;
import com.atlassian.fusion.aci.api.service.ACIRegistrationService;
import com.atlassian.jira.plugins.dvcs.service.optional.ServiceAccessor;
import com.atlassian.jira.plugins.dvcs.service.optional.aci.ACIRegistrationServiceAccessor;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.io.IOUtils;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Optional.empty;
import static java.util.Optional.of;

@Named
@ExportAsService(LifecycleAware.class)
public class AciDescriptorRegistrationTask implements LifecycleAware {
    @VisibleForTesting
    static final String DVCS_CONNECT_DESCRIPTOR_JSON_LOCATION = "/connect/dvcs-connect-descriptor.json";
    private static final Logger LOGGER = LoggerFactory.getLogger(AciDescriptorRegistrationTask.class);
    private final BundleContext bundleContext;

    @Inject
    @SuppressWarnings("SpringJavaAutowiringInspection")
    public AciDescriptorRegistrationTask(final BundleContext bundleContext) {
        this.bundleContext = checkNotNull(bundleContext);
    }

    @Override
    public void onStart() {
        new ACIRegistrationThunk(bundleContext).apply();
    }

    @Override
    public void onStop() {
    }

    @VisibleForTesting
    static class ACIRegistrationThunk {
        private final ServiceAccessor<ACIRegistrationService> serviceAccessor;

        ACIRegistrationThunk(@Nonnull final BundleContext bundleContext) {
            this(new ACIRegistrationServiceAccessor(checkNotNull(bundleContext)));
        }

        ACIRegistrationThunk(@Nonnull final ServiceAccessor<ACIRegistrationService> serviceAccessor) {
            this.serviceAccessor = checkNotNull(serviceAccessor);
        }

        public Optional<ConnectApplication> apply() {
            return serviceAccessor.get().flatMap(
                    service -> getDescriptorJson().map(
                            descriptor -> service.register(BITBUCKET_CONNECTOR_APPLICATION_ID, descriptor,
                                    RemoteApplication.BITBUCKET)));
        }

        private Optional<String> getDescriptorJson() {
            try (InputStream descriptorStream = this.getClass().getResourceAsStream(DVCS_CONNECT_DESCRIPTOR_JSON_LOCATION)) {
                return of(IOUtils.toString(descriptorStream));
            } catch (IOException e) {
                // Should we just throw a runtime exception here? If we can't get this from the class path we are in trouble.
                LOGGER.error("DVCS Connector failed to retrieve connect descriptor from classpath", e);
                return empty();
            }
        }
    }
}
