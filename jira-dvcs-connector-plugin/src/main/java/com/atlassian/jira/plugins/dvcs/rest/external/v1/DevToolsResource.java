package com.atlassian.jira.plugins.dvcs.rest.external.v1;

import com.atlassian.adapter.jackson.ObjectMapper;
import com.atlassian.annotations.ExperimentalApi;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugins.dvcs.model.Changeset;
import com.atlassian.jira.plugins.dvcs.model.DvcsUser;
import com.atlassian.jira.plugins.dvcs.model.Participant;
import com.atlassian.jira.plugins.dvcs.model.PullRequest;
import com.atlassian.jira.plugins.dvcs.model.PullRequestRef;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.model.dev.RestChangeset;
import com.atlassian.jira.plugins.dvcs.model.dev.RestChangesetRepository;
import com.atlassian.jira.plugins.dvcs.model.dev.RestDevResponse;
import com.atlassian.jira.plugins.dvcs.model.dev.RestParticipant;
import com.atlassian.jira.plugins.dvcs.model.dev.RestPrCommit;
import com.atlassian.jira.plugins.dvcs.model.dev.RestPrRepository;
import com.atlassian.jira.plugins.dvcs.model.dev.RestPullRequest;
import com.atlassian.jira.plugins.dvcs.model.dev.RestRef;
import com.atlassian.jira.plugins.dvcs.model.dev.RestRepository;
import com.atlassian.jira.plugins.dvcs.model.dev.RestUser;
import com.atlassian.jira.plugins.dvcs.rest.security.AdminOnly;
import com.atlassian.jira.plugins.dvcs.rest.security.AuthorizationException;
import com.atlassian.jira.plugins.dvcs.service.BranchService;
import com.atlassian.jira.plugins.dvcs.service.ChangesetService;
import com.atlassian.jira.plugins.dvcs.service.PullRequestService;
import com.atlassian.jira.plugins.dvcs.service.RepositoryService;
import com.atlassian.jira.plugins.dvcs.webwork.IssueAndProjectKeyManager;
import com.atlassian.jira.project.Project;
import com.atlassian.plugins.rest.common.Status;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimaps;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.permission.ProjectPermissions.BROWSE_PROJECTS;
import static com.atlassian.jira.software.api.permissions.SoftwareProjectPermissions.VIEW_DEV_TOOLS;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonMap;
import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static javax.ws.rs.core.Response.ok;

/**
 * The DevTools Resource.
 */
@Path("/jira-dev")
public class DevToolsResource {
    private final BranchService branchService;
    private final ChangesetService changesetService;
    private final IssueAndProjectKeyManager issueAndProjectKeyManager;
    private final PullRequestService pullRequestService;
    private final RepositoryService repositoryService;

    public DevToolsResource(
            final BranchService branchService,
            final ChangesetService changesetService,
            final IssueAndProjectKeyManager issueAndProjectKeyManager,
            final PullRequestService pullRequestService,
            final RepositoryService repositoryService) {
        this.branchService = checkNotNull(branchService);
        this.changesetService = checkNotNull(changesetService);
        this.issueAndProjectKeyManager = checkNotNull(issueAndProjectKeyManager);
        this.pullRequestService = checkNotNull(pullRequestService);
        this.repositoryService = checkNotNull(repositoryService);
    }

    @GET
    @Path("/commits")
    @Produces(APPLICATION_JSON)
    @AdminOnly
    @ExperimentalApi
    public Response getCommitDetails(@QueryParam("issue") final String issueKey) {
        final List<Changeset> changesets = changesetService.getByIssueKey(singleton(issueKey), true);
        final List<Changeset> changesetsWithFileDetails = changesetService.getChangesetsWithFileDetails(changesets);
        final String json = new ObjectMapper().writeValueAsString(changesetsWithFileDetails);
        return ok(json, APPLICATION_JSON_TYPE).build();
    }

    @GET
    @Path("/detail")
    @Produces(APPLICATION_JSON)
    public Response getCommits(@QueryParam("issue") String issueKey) {
        return new RestTransformer<RestChangesetRepository>() {
            private ListMultimap<Integer, Changeset> changesetTorepositoryMapping;

            @Override
            protected Set<Integer> getRepositories(final Set<String> issueKeys) {
                List<Changeset> changesets = changesetService.getByIssueKey(issueKeys, true);

                changesetTorepositoryMapping = ArrayListMultimap.create();

                // group changesets by repository
                for (Changeset changeset : changesets) {
                    for (int repositoryId : changeset.getRepositoryIds()) {
                        changesetTorepositoryMapping.put(repositoryId, changeset);
                    }
                }

                return changesetTorepositoryMapping.keySet();
            }

            @Override
            protected RestChangesetRepository createRepository() {
                return new RestChangesetRepository();
            }

            @Override
            protected void setData(final RestChangesetRepository restRepository, final Repository repository) {
                restRepository.setCommits(createCommits(repository, changesetTorepositoryMapping.get(repository.getId())));
            }

        }.getResponse(issueKey);
    }

    private List<RestChangeset> createCommits(Repository repository, List<Changeset> changesets) {
        return changesets.stream().map(changeset -> {
            final DvcsUser user = repositoryService.getUser(repository, changeset.getAuthor(), changeset.getRawAuthor());
            final RestChangeset restChangeset = new RestChangeset();
            restChangeset.setAuthor(new RestUser(user.getUsername(), user.getFullName(), changeset.getAuthorEmail(), user.getAvatar()));
            restChangeset.setAuthorTimestamp(changeset.getDate().getTime());
            restChangeset.setDisplayId(changeset.getNode().substring(0, 7));
            restChangeset.setId(changeset.getRawNode());
            restChangeset.setMessage(changeset.getMessage());
            restChangeset.setFileCount(changeset.getAllFileCount());
            restChangeset.setUrl(changesetService.getCommitUrl(repository, changeset));

            if (changeset.getParents() == null) {
                // no parents are set, it means that the length of the parent json is too long, so it was large merge (e.g Octopus merge)
                restChangeset.setMerge(true);
            } else {
                restChangeset.setMerge(changeset.getParents().size() > 1);
            }

            return restChangeset;

        }).collect(toList());
    }

    @GET
    @Path("/pr-detail")
    @Produces(APPLICATION_JSON)
    public Response getPullRequests(@QueryParam("issue") String issueKey) {
        return new RestTransformer<RestPrRepository>() {
            private ListMultimap<Integer, PullRequest> prTorepositoryMapping;

            @Override
            protected Set<Integer> getRepositories(final Set<String> issueKeys) {
                final List<PullRequest> pullRequests = pullRequestService.getByIssueKeys(issueKeys, true);
                prTorepositoryMapping = Multimaps.index(pullRequests, PullRequest::getRepositoryId);
                return prTorepositoryMapping.keySet();
            }

            @Override
            protected RestPrRepository createRepository() {
                return new RestPrRepository();
            }

            @Override
            protected void setData(final RestPrRepository restRepository, final Repository repository) {
                restRepository.setPullRequests(
                        createPullRequests(repository, prTorepositoryMapping.get(repository.getId())));
            }

        }.getResponse(issueKey);
    }

    private List<RestPullRequest> createPullRequests(final Repository repository, final List<PullRequest> pullRequests) {
        final List<RestPullRequest> restPullRequests = new ArrayList<>();
        for (final PullRequest pullRequest : pullRequests) {
            final DvcsUser user = repositoryService.getUser(repository, pullRequest.getAuthor(), pullRequest.getAuthor());
            final RestPullRequest restPullRequest = new RestPullRequest();
            restPullRequest.setAuthor(new RestUser(user.getUsername(), user.getFullName(), null, user.getAvatar()));
            restPullRequest.setCreatedOn(pullRequest.getCreatedOn().getTime());
            restPullRequest.setTitle(pullRequest.getName());
            restPullRequest.setId(pullRequest.getRemoteId());
            restPullRequest.setUrl(pullRequest.getUrl());
            restPullRequest.setUpdatedOn(pullRequest.getUpdatedOn().getTime());
            restPullRequest.setStatus(pullRequest.getStatus().name());
            restPullRequest.setSource(createRef(pullRequest.getSource()));
            restPullRequest.setDestination(createRef(pullRequest.getDestination()));
            restPullRequests.add(restPullRequest);
            restPullRequest.setParticipants(createParticipants(repository, pullRequest.getParticipants()));
            restPullRequest.setCommentCount(pullRequest.getCommentCount());
            restPullRequest.setCommits(createPrCommits(pullRequest.getCommits()));
        }

        return restPullRequests;
    }

    private List<RestParticipant> createParticipants(final Repository repository, final List<Participant> participants) {
        return participants.stream().map(participant -> {
            DvcsUser user = repositoryService.getUser(repository, participant.getUsername(), participant.getUsername());
            final RestUser restUser = new RestUser(user.getUsername(), user.getFullName(), null, user.getAvatar());
            return new RestParticipant(restUser, participant.isApproved(), participant.getRole());
        }).collect(toList());
    }

    private RestRef createRef(PullRequestRef ref) {
        if (ref == null) {
            return null;
        }
        final RestRef restRef = new RestRef();
        restRef.setBranch(ref.getBranch());
        restRef.setRepository(ref.getRepository());
        restRef.setUrl(ref.getRepositoryUrl());
        return restRef;
    }

    private List<RestPrCommit> createPrCommits(final List<Changeset> prCommits) {
        if (prCommits == null) {
            return null;
        }
        return prCommits.stream().map(prCommit -> {
            final RestPrCommit restPrCommit = new RestPrCommit();
            restPrCommit.setRawAuthor(prCommit.getRawAuthor());
            restPrCommit.setAuthor(prCommit.getAuthor());
            restPrCommit.setDate(prCommit.getDate());
            restPrCommit.setMessage(prCommit.getMessage());
            restPrCommit.setNode(prCommit.getNode());
            return restPrCommit;

        }).collect(toList());
    }

    @GET
    @Path("/branch")
    @Produces(APPLICATION_JSON)
    public Response getBranches(@QueryParam("issue") String issueKey) {
        final Issue issue = issueAndProjectKeyManager.getIssue(issueKey);
        if (issue == null) {
            return Status.notFound().message("Issue not found").response();
        }

        if (!issueAndProjectKeyManager.hasIssuePermission(BROWSE_PROJECTS, issue)) {
            throw new AuthorizationException();
        }

        final Project project = issue.getProjectObject();

        if (project == null) {
            return Status.notFound().message("Project was not found").response();
        }

        if (!issueAndProjectKeyManager.hasProjectPermission(VIEW_DEV_TOOLS, project)) {
            throw new AuthorizationException();
        }

        final Set<String> issueKeys = issueAndProjectKeyManager.getAllIssueKeys(issue);

        final Map<?, ?> result = singletonMap("branches", branchService.getByIssueKey(issueKeys));
        return ok(result).build();
    }

    private abstract class RestTransformer<T extends RestRepository> {
        public Response getResponse(String issueKey) {
            Issue issue = issueAndProjectKeyManager.getIssue(issueKey);
            if (issue == null) {
                return Status.notFound().message("Issue not found").response();
            }

            if (!issueAndProjectKeyManager.hasIssuePermission(BROWSE_PROJECTS, issue)) {
                throw new AuthorizationException();
            }

            Project project = issue.getProjectObject();

            if (project == null) {
                return Status.notFound().message("Project was not found").response();
            }

            if (!issueAndProjectKeyManager.hasProjectPermission(VIEW_DEV_TOOLS, project)) {
                throw new AuthorizationException();
            }

            final Set<String> issueKeys = issueAndProjectKeyManager.getAllIssueKeys(issue);
            final Set<Integer> repositoryIds = getRepositories(issueKeys);
            final Map<Integer, Repository> repositories = new HashMap<>();

            final List<T> restRepositories = new ArrayList<>();
            for (final int repositoryId : repositoryIds) {
                Repository repository = repositories.get(repositoryId);

                if (repository == null) {
                    repository = repositoryService.get(repositoryId);
                    repositories.put(repositoryId, repository);
                }

                final T restRepository = createRepository();
                restRepository.setName(repository.getName());
                restRepository.setSlug(repository.getSlug());
                restRepository.setUrl(repository.getRepositoryUrl());
                restRepository.setAvatar(repository.getLogo());
                setData(restRepository, repository);
                restRepository.setFork(repository.isFork());
                if (repository.isFork() && repository.getForkOf() != null) {
                    final RestRepository forkOfRepository = new RestChangesetRepository();
                    forkOfRepository.setName(repository.getForkOf().getName());
                    forkOfRepository.setSlug(repository.getForkOf().getSlug());
                    forkOfRepository.setUrl(repository.getForkOf().getRepositoryUrl());
                    restRepository.setForkOf(forkOfRepository);
                }

                restRepositories.add(restRepository);
            }

            final RestDevResponse<T> result = new RestDevResponse<>();
            result.setRepositories(restRepositories);
            return ok(result).build();
        }

        protected abstract Set<Integer> getRepositories(final Set<String> issueKeys);

        protected abstract T createRepository();

        protected abstract void setData(T restRepository, Repository repository);
    }
}
