package com.atlassian.jira.plugins.dvcs.ondemand;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.google.common.base.Preconditions.checkNotNull;

@Component
@ExportAsService(LifecycleAware.class)
public class AccountsConfigLifecycler implements LifecycleAware {
    private final AccountsConfigService configService;

    @Autowired
    public AccountsConfigLifecycler(final AccountsConfigService configService) {
        this.configService = checkNotNull(configService);
    }

    @Override
    public void onStart() {
        configService.scheduleReload();
    }

    @Override
    public void onStop() {
    }
}
