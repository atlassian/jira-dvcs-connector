package com.atlassian.jira.plugins.dvcs.dao.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.BranchHeadMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.BranchMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.IssueToBranchMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.OrganizationMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.RepositoryMapping;
import com.atlassian.jira.plugins.dvcs.model.Branch;
import com.atlassian.jira.plugins.dvcs.model.BranchHead;
import com.atlassian.jira.plugins.dvcs.util.ActiveObjectsUtils;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.ObjectArrays;
import net.java.ao.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.jira.plugins.dvcs.dao.impl.DAOConstants.MAXIMUM_ENTITIES_PER_ISSUE_KEY;
import static com.atlassian.jira.plugins.dvcs.util.CustomStringUtils.stripToLimit;
import static com.google.common.base.Preconditions.checkNotNull;

@Named
public class BranchDaoImpl {
    private static final Logger log = LoggerFactory.getLogger(BranchDaoImpl.class);

    private final ActiveObjects activeObjects;

    @Inject
    public BranchDaoImpl(@ComponentImport ActiveObjects activeObjects) {
        this.activeObjects = checkNotNull(activeObjects);
    }

    public List<BranchHead> getBranchHeads(int repositoryId) {
        BranchHeadMapping[] result = activeObjects.find(BranchHeadMapping.class, Query.select().where(BranchHeadMapping.REPOSITORY_ID + " = ?", repositoryId));

        return Lists.transform(Arrays.asList(result), input -> new BranchHead(input.getBranchName(), input.getHead()));
    }

    public List<Branch> getBranches(final int repositoryId) {
        BranchMapping[] result = activeObjects.find(BranchMapping.class, Query.select().where(BranchMapping.REPOSITORY_ID + " = ?", repositoryId));

        return Lists.transform(Arrays.asList(result), input -> new Branch(input.getID(), input.getName(), repositoryId));
    }

    public void createBranch(final int repositoryId, final Branch branch, final Set<String> issueKeys) {
        activeObjects.executeInTransaction(() -> {
            log.debug("adding branch {} for repository with id = [ {} ]", new Object[]{branch, repositoryId});
            final Map<String, Object> map = new MapRemovingNullCharacterFromStringValues();
            map.put(BranchMapping.REPOSITORY_ID, repositoryId);
            map.put(BranchMapping.NAME, stripToLimit(branch.getName(), 255));

            BranchMapping branchMapping = activeObjects.create(BranchMapping.class, map);
            associateBranchToIssue(branchMapping, issueKeys);
            return null;
        });
    }

    public void createBranchHead(final int repositoryId, final BranchHead branchHead) {
        activeObjects.executeInTransaction(() -> {
            log.debug("adding branch head {} for repository with id = [ {} ]", new Object[]{branchHead, repositoryId});
            final Map<String, Object> map = new MapRemovingNullCharacterFromStringValues();
            map.put(BranchHeadMapping.REPOSITORY_ID, repositoryId);
            map.put(BranchHeadMapping.BRANCH_NAME, stripToLimit(branchHead.getName(), 255));
            map.put(BranchHeadMapping.HEAD, branchHead.getHead());

            activeObjects.create(BranchHeadMapping.class, map);
            return null;
        });
    }

    public void removeBranchHead(final int repositoryId, final BranchHead branch) {
        activeObjects.executeInTransaction(() -> {
            log.debug("deleting branch head {} for repository with id = [ {} ]", new Object[]{branch, repositoryId});
            activeObjects.deleteWithSQL(BranchHeadMapping.class, BranchHeadMapping.REPOSITORY_ID + " = ? AND "
                    + BranchHeadMapping.BRANCH_NAME + " = ? AND "
                    + BranchHeadMapping.HEAD + " = ?", repositoryId, branch.getName(), branch.getHead());
            return null;
        });
    }

    public void removeAllBranchHeadsInRepository(final int repositoryId) {
        log.debug("deleting branch heads for repository with id = [ {} ]", repositoryId);

        activeObjects.executeInTransaction(() -> {
            activeObjects.deleteWithSQL(BranchHeadMapping.class, BranchHeadMapping.REPOSITORY_ID + " = ?", repositoryId);
            return null;
        });
    }

    public List<Branch> getBranchesForIssue(final Iterable<String> issueKeys) {
        final String baseWhereClause = ActiveObjectsUtils.renderListOperator("mapping." + IssueToBranchMapping.ISSUE_KEY, "IN", "OR", issueKeys);
        final Object[] params = ObjectArrays.concat(new Object[]{Boolean.FALSE, Boolean.TRUE}, Iterables.toArray(issueKeys, Object.class), Object.class);

        List<BranchMapping> branches = activeObjects.executeInTransaction(() -> {
            BranchMapping[] mappings = activeObjects.find(BranchMapping.class,
                    Query.select()
                            .alias(IssueToBranchMapping.class, "mapping")
                            .alias(BranchMapping.class, "branch")
                            .alias(RepositoryMapping.class, "repo")
                            .join(IssueToBranchMapping.class, "mapping." + IssueToBranchMapping.BRANCH_ID + " = branch.ID")
                            .join(RepositoryMapping.class, "branch." + BranchMapping.REPOSITORY_ID + " = repo.ID")
                            .where("repo." + RepositoryMapping.DELETED + " = ? AND repo." + RepositoryMapping.LINKED + " = ? AND " + baseWhereClause, params)
                            .order(BranchMapping.NAME)
                            .limit(MAXIMUM_ENTITIES_PER_ISSUE_KEY));

            return Arrays.asList(mappings);
        });

        return Lists.transform(branches, input -> new Branch(input.getID(), input.getName(), input.getRepository().getID()));
    }

    public List<Branch> getBranchesForIssue(final Iterable<String> issueKeys, final String dvcsType) {
        final String baseWhereClause = ActiveObjectsUtils.renderListOperator("mapping." + IssueToBranchMapping.ISSUE_KEY, "IN", "OR", issueKeys);
        final Object[] params = ObjectArrays.concat(new Object[]{dvcsType, Boolean.FALSE, Boolean.TRUE}, Iterables.toArray(issueKeys, Object.class), Object.class);

        final List<BranchMapping> branches = activeObjects.executeInTransaction(() -> {
            BranchMapping[] mappings = activeObjects.find(BranchMapping.class,
                    Query.select()
                            .alias(IssueToBranchMapping.class, "mapping")
                            .alias(BranchMapping.class, "branch")
                            .alias(RepositoryMapping.class, "repo")
                            .alias(OrganizationMapping.class, "org")
                            .join(IssueToBranchMapping.class, "mapping." + IssueToBranchMapping.BRANCH_ID + " = branch.ID")
                            .join(RepositoryMapping.class, "branch." + BranchMapping.REPOSITORY_ID + " = repo.ID")
                            .join(OrganizationMapping.class, "repo." + RepositoryMapping.ORGANIZATION_ID + " = org.ID")
                            .where("org." + OrganizationMapping.DVCS_TYPE + " = ? AND repo." + RepositoryMapping.DELETED + " = ? AND repo." + RepositoryMapping.LINKED + " = ? AND " + baseWhereClause,
                                    params)
                            .order(BranchMapping.NAME)
                            .limit(MAXIMUM_ENTITIES_PER_ISSUE_KEY));

            return Arrays.asList(mappings);
        });

        return Lists.transform(branches, input -> new Branch(input.getID(), input.getName(), input.getRepository().getID()));
    }

    public List<Branch> getBranchesForRepository(final int repositoryId) {
        final List<BranchMapping> branches = activeObjects.executeInTransaction(() -> {
            BranchMapping[] mappings = activeObjects.find(BranchMapping.class,
                    Query.select()
                            .from(BranchMapping.class)
                            .alias(BranchMapping.class, "branch")
                            .alias(RepositoryMapping.class, "repo")
                            .join(RepositoryMapping.class, "branch." + BranchMapping.REPOSITORY_ID + " = repo.ID")
                            .where("repo." + RepositoryMapping.DELETED + " = ? AND repo." + RepositoryMapping.LINKED + " = ? AND branch." + BranchMapping.REPOSITORY_ID + " = ?", Boolean.FALSE, Boolean.TRUE, repositoryId));

            return Arrays.asList(mappings);
        });

        return Lists.transform(branches, input -> new Branch(input.getID(), input.getName(), input.getRepository().getID()));
    }

    private void associateBranchToIssue(BranchMapping branchMapping, Set<String> extractedIssues) {
        // remove all assoc issues-branch
        activeObjects.deleteWithSQL(IssueToBranchMapping.class, IssueToBranchMapping.BRANCH_ID + " = ? ", branchMapping);
        // insert all
        for (String extractedIssue : extractedIssues) {
            final Map<String, Object> map = new MapRemovingNullCharacterFromStringValues();
            map.put(IssueToBranchMapping.ISSUE_KEY, extractedIssue);
            map.put(IssueToBranchMapping.BRANCH_ID, branchMapping.getID());
            activeObjects.create(IssueToBranchMapping.class, map);
        }
    }
}
