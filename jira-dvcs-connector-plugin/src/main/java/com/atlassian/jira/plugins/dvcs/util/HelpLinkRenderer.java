package com.atlassian.jira.plugins.dvcs.util;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * Server-side rendering of help links with the correct Confluence URL and space name
 * for both Cloud and Server, using the {@link com.atlassian.jira.help.HelpUrls} API.
 *
 * @since 3.4.0
 */
@ParametersAreNonnullByDefault
public interface HelpLinkRenderer {

    /**
     * Renders the help link with the given key.
     *
     * @param key the key of the help link to render, for example "link.gh.account.with.jira" in help-admin.properties
     * @return an HTML anchor tag
     */
    @Nonnull
    String render(String key);
}
