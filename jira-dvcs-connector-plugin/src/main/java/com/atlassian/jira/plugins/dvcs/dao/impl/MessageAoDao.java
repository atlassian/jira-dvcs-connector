package com.atlassian.jira.plugins.dvcs.dao.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageQueueItemMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageTagMapping;
import com.atlassian.jira.plugins.dvcs.ao.QueryHelper;
import com.atlassian.jira.plugins.dvcs.model.MessageState;
import com.atlassian.jira.plugins.dvcs.util.ao.QueryTemplate;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.DBParam;
import net.java.ao.Query;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Map;

import static com.atlassian.jira.plugins.dvcs.util.ActiveObjectsUtils.ID;

/**
 * The active objects DAO that the QueryDsl DAO delegates to for methods it has not implemented yet.
 * It implements a subset of the MessageDAO interface contract
 */
@Component
public class MessageAoDao {

    private static final MessageState[] MESSAGE_STATES = new MessageState[]{MessageState.PENDING,
            MessageState.RUNNING,
            MessageState.SLEEPING};

    @Resource
    @ComponentImport
    private ActiveObjects activeObjects;

    @Resource
    private QueryHelper queryHelper;

    public MessageMapping create(final Map<String, Object> message, final String[] tags) {
        return activeObjects.executeInTransaction(() -> {
            MessageMapping result = activeObjects.create(MessageMapping.class, message);
            for (String tag : tags) {
                activeObjects.create(MessageTagMapping.class,
                        new DBParam(MessageTagMapping.MESSAGE, result.getID()),
                        new DBParam(MessageTagMapping.TAG, tag)
                );
            }
            return result;
        });
    }

    public MessageTagMapping createTag(final int messageId, final String tag) {
        return activeObjects.executeInTransaction(() -> activeObjects.create(MessageTagMapping.class,
                new DBParam(MessageTagMapping.MESSAGE, messageId),
                new DBParam(MessageTagMapping.TAG, tag)));
    }

    public void delete(final MessageMapping message) {
        activeObjects.executeInTransaction(() -> {
            if (message.getTags() != null) {
                Arrays.stream(message.getTags()).forEach(activeObjects::delete);
            }
            activeObjects.delete(message);
            return null;
        });
    }

    public MessageMapping getById(final int id) {
        return activeObjects.get(MessageMapping.class, id);
    }


    public int getMessagesForConsumingCount(final String tag) {
        Query query = new QueryTemplate(queryHelper) {

            @Override
            protected void build() {
                alias(MessageMapping.class, "messageMapping");
                alias(MessageTagMapping.class, "messageTag");
                alias(MessageQueueItemMapping.class, "messageQueueItem");

                join(MessageTagMapping.class, column(MessageMapping.class, ID), MessageTagMapping.MESSAGE);
                join(MessageQueueItemMapping.class, column(MessageMapping.class, ID), MessageQueueItemMapping.MESSAGE);

                where(and( //
                        eq(column(MessageTagMapping.class, MessageTagMapping.TAG), parameter("tag")), //
                        in(column(MessageQueueItemMapping.class, MessageQueueItemMapping.STATE), parameter("state")) //
                ));
            }

        }.toQuery(MapBuilder.<String, Object>build("tag", tag, "state", MESSAGE_STATES));
        return activeObjects.count(MessageMapping.class, query);
    }
}
