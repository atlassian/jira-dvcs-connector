package com.atlassian.jira.plugins.dvcs.navlinks;

import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.service.OrganizationService;
import com.atlassian.plugins.navlink.consumer.menu.services.NavigationLinkComparator;
import com.atlassian.plugins.navlink.producer.navigation.services.NavigationLinkRepository;
import com.atlassian.plugins.navlink.producer.navigation.services.RawNavigationLink;
import com.atlassian.plugins.navlink.producer.navigation.services.RawNavigationLinkBuilder;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Predicate;

import javax.annotation.Nonnull;
import java.util.List;

import static com.atlassian.jira.plugins.dvcs.model.Organization.ApprovalState.APPROVED;
import static com.atlassian.plugins.navlink.producer.navigation.links.LinkSource.remoteDefault;
import static com.atlassian.plugins.navlink.producer.navigation.links.NavigationLinkBase.HOME_APPS_KEY;
import static com.google.common.base.MoreObjects.firstNonNull;
import static com.google.common.base.Predicates.alwaysTrue;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

/**
 * A NavigationLinkRepository that provides links to connected Bitbucket Cloud accounts.
 *
 * @since 3.5.0
 */
public class BitbucketCloudAccountNavLinkRepository implements NavigationLinkRepository {

    @VisibleForTesting
    static final String BITBUCKET_ORG_TYPE = "bitbucket";

    private static final int WEIGHT = NavigationLinkComparator.Weights.MAX.value();

    private static final String APPLICATION_TYPE = "jira";

    /**
     * Creates a nav link for the given organization (DVCS account).
     *
     * @param organization the org for which to create a link
     * @return see above
     */
    @Nonnull
    private static RawNavigationLink toNavLink(@Nonnull final Organization organization) {
        return new RawNavigationLinkBuilder()
                .applicationType(APPLICATION_TYPE)
                .href(organization.getOrganizationUrl())
                .key(HOME_APPS_KEY)
                .labelKey(getLinkLabel(organization))
                .self(false)
                .source(remoteDefault())
                .weight(WEIGHT)
                .build();
    }

    /**
     * Generates the user-facing label for a link to the given Bitbucket Cloud account.
     *
     * @param organization the org to which the link points
     * @return the label
     */
    @Nonnull
    @VisibleForTesting
    static String getLinkLabel(@Nonnull final Organization organization) {
        return "Bitbucket - " + organization.getName();
    }

    private final OrganizationService organizationService;

    public BitbucketCloudAccountNavLinkRepository(@Nonnull final OrganizationService organizationService) {
        this.organizationService = requireNonNull(organizationService);
    }

    @Override
    public Iterable<RawNavigationLink> all() {
        return matching(alwaysTrue());
    }

    @Override
    public Iterable<RawNavigationLink> matching(final Predicate<RawNavigationLink> nullablePredicate) {
        final Predicate<RawNavigationLink> navLinkPredicate = firstNonNull(nullablePredicate, alwaysTrue());
        final List<Organization> bitbucketAccounts = organizationService.getAll(false, BITBUCKET_ORG_TYPE);
        return bitbucketAccounts.stream()
                .filter(account -> APPROVED.equals(account.getApprovalState()))
                .map(BitbucketCloudAccountNavLinkRepository::toNavLink)
                .filter(navLinkPredicate::apply)
                .collect(toList());
    }
}
