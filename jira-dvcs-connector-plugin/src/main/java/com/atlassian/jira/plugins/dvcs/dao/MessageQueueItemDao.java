package com.atlassian.jira.plugins.dvcs.dao;

import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.MessageQueueItemMapping;
import com.atlassian.jira.plugins.dvcs.model.Message;
import com.atlassian.jira.plugins.dvcs.model.MessageId;
import com.atlassian.jira.plugins.dvcs.model.MessageQueueItem;
import com.atlassian.jira.plugins.dvcs.model.MessageState;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * DAO layer related to MessageQueueItems
 */
@ParametersAreNonnullByDefault
public interface MessageQueueItemDao {
    /**
     * @param queueItem to create
     * @return created {@link MessageQueueItemMapping}
     */
    MessageQueueItem create(final Map<String, Object> queueItem);

    /**
     * Saves the state of the messageQueueItem to the DB
     *
     * @param messageQueueItem to save/update
     */
    void save(MessageQueueItem messageQueueItem);

    /**
     * Deletes a messageQueueItem
     *
     * @param messageQueueItem to delete
     */
    void delete(MessageQueueItem messageQueueItem);

    /**
     * Deletes the record with the given ID number
     *
     * @param messageQueueItemId to delete
     */
    void delete(int messageQueueItemId);

    /**
     * Get all queue items that match a message ID
     *
     * @param messageId {@link MessageMapping#getID()}
     * @return founded queue items
     */
    MessageQueueItem[] getByMessageId(MessageId messageId);

    /**
     * Gets the messageQueueItem associated with the message queue id
     *
     * @param id the id we want a queue item for
     * @return a queue item for provided queue name and message id
     */
    Optional<MessageQueueItem> getQueueItemById(int id);

    /**
     * Gets a message queue item that matches the queue and message id
     *
     * @param queue     name of queue
     * @param messageId {@link MessageQueueItemDao}
     * @return a queue item for provided queue name and message id
     */
    Optional<MessageQueueItem> getByQueueAndMessage(String queue, int messageId);

    /**
     * applies a consumer function to all queue item IDs that match a given tag or state
     *
     * @param tag      {@link Message#getTags()}
     * @param state    {@link MessageQueueItemMapping#getState()}
     * @param consumer the function to apply on each resulting queue item ID
     */
    void getByTagAndState(String tag, MessageState state, Consumer<Integer> consumer);

    /**
     * applies a consumer function to all queue item IDs that match a given state
     *
     * @param state    for which state
     * @param consumer the function to apply on each resulting queue item ID
     */
    void getByState(MessageState state, Consumer<Integer> consumer);

    /**
     * @param queue   name of queue
     * @param address of messages
     * @return next message for consuming or null, if queue is already empty
     */
    Message getNextItemForProcessing(String queue, String address);

}
