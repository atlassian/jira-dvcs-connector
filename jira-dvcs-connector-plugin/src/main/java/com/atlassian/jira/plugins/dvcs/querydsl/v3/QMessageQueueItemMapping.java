package com.atlassian.jira.plugins.dvcs.querydsl.v3;

import com.atlassian.pocketknife.spi.querydsl.EnhancedRelationalPathBase;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.PrimaryKey;

import java.util.Date;

public class QMessageQueueItemMapping extends EnhancedRelationalPathBase<QMessageQueueItemMapping> {
    private static final String AO_TABLE_NAME = "AO_E8B6CC_MESSAGE_QUEUE_ITEM";
    private static final long serialVersionUID = -898688883697761766L;

    public final NumberPath<Integer> ID = createInteger("ID");
    public final DateTimePath<Date> LAST_FAILED = createDateTime("LAST_FAILED", Date.class);
    public final NumberPath<Integer> MESSAGE_ID = createIntegerCol("MESSAGE_ID").notNull().build();
    public final NumberPath<Integer> RETRIES_COUNT = createInteger("RETRIES_COUNT");
    public final StringPath STATE = createString("STATE");
    public final StringPath STATE_INFO = createString("STATE_INFO");
    public final StringPath QUEUE = createString("QUEUE");

    public final PrimaryKey<QMessageQueueItemMapping> MESSAGE_QUEUE_ITEM_PK = createPrimaryKey(ID);

    public QMessageQueueItemMapping() {
        super(QMessageQueueItemMapping.class, AO_TABLE_NAME);
    }

}