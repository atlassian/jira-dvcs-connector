package com.atlassian.jira.plugins.dvcs.service;

import com.atlassian.beehive.ClusterLockService;
import com.atlassian.fusion.aci.api.model.Installation;
import com.atlassian.fusion.aci.api.service.ACIInstallationService;
import com.atlassian.jira.plugins.dvcs.activity.RepositoryPullRequestDao;
import com.atlassian.jira.plugins.dvcs.analytics.AnalyticsService;
import com.atlassian.jira.plugins.dvcs.analytics.smartcommits.SmartCommitsAnalyticsService;
import com.atlassian.jira.plugins.dvcs.dao.OrganizationDao;
import com.atlassian.jira.plugins.dvcs.dao.RepositoryDao;
import com.atlassian.jira.plugins.dvcs.dao.SyncAuditLogDao;
import com.atlassian.jira.plugins.dvcs.event.CarefulEventService;
import com.atlassian.jira.plugins.dvcs.exception.SourceControlException;
import com.atlassian.jira.plugins.dvcs.model.DefaultProgress;
import com.atlassian.jira.plugins.dvcs.model.DvcsUser;
import com.atlassian.jira.plugins.dvcs.model.DvcsUser.UnknownUser;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Progress;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.model.RepositoryRegistration;
import com.atlassian.jira.plugins.dvcs.model.credential.Credential;
import com.atlassian.jira.plugins.dvcs.model.credential.PrincipalIDCredential;
import com.atlassian.jira.plugins.dvcs.service.optional.aci.AciInstallationServiceAccessor;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicator;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicatorProvider;
import com.atlassian.jira.plugins.dvcs.spi.github.service.GitHubEventService;
import com.atlassian.jira.plugins.dvcs.sync.SynchronizationFlag;
import com.atlassian.jira.plugins.dvcs.sync.Synchronizer;
import com.atlassian.jira.plugins.dvcs.util.ExceptionLogger;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Maps;
import io.atlassian.fugue.Option;
import org.apache.commons.lang3.BooleanUtils;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.locks.Lock;

import static com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicator.POST_HOOK_SUFFIX;
import static com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID;
import static com.atlassian.jira.plugins.dvcs.sync.SynchronizationFlag.SYNC_CHANGESETS;
import static com.atlassian.jira.plugins.dvcs.sync.SynchronizationFlag.SYNC_PULL_REQUESTS;
import static com.atlassian.jira.plugins.dvcs.util.DvcsConstants.LINKERS_ENABLED_SETTINGS_PARAM;
import static com.google.common.base.Strings.isNullOrEmpty;
import static io.atlassian.fugue.Option.fromOptional;
import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

@Component
public class RepositoryServiceImpl implements RepositoryService {
    @VisibleForTesting
    static final String SYNC_REPOSITORY_LIST_LOCK = RepositoryService.class.getName() + ".syncRepositoryList";

    private static final Logger log = ExceptionLogger.getLogger(RepositoryServiceImpl.class);

    @Resource
    private DvcsCommunicatorProvider communicatorProvider;

    @Resource
    private OrganizationDao organizationDao;

    @Resource
    private RepositoryDao repositoryDao;

    @Resource
    private RepositoryPullRequestDao repositoryPullRequestDao;

    @Resource
    private Synchronizer synchronizer;

    @Resource
    private ChangesetService changesetService;

    @Resource
    private BranchService branchService;

    @Resource
    private LinkerService linkerService;

    @Resource
    @ComponentImport
    private ApplicationProperties applicationProperties;

    @Resource
    @ComponentImport
    private PluginSettingsFactory pluginSettingsFactory;

    @Resource
    private GitHubEventService gitHubEventService;

    @Resource
    private SyncAuditLogDao syncAuditDao;

    @Resource
    private CarefulEventService eventService;

    @Resource
    private SmartCommitsAnalyticsService smartCommitsAnalyticsService;

    @Resource
    private AciInstallationServiceAccessor aciInstallationServiceAccessor;

    @Resource
    @ComponentImport
    private ClusterLockService clusterLockService;

    @Resource
    private AnalyticsService analyticsService;

    @Resource
    private DvcsConnectorExecutorFactory executorFactory;

    private ThreadPoolExecutor repositoryDeletionExecutor;
    private ThreadPoolExecutor webhookRemovalExecutor;

    private static void logFailedWebhook(final Repository repository, final boolean adding, final Exception e) {
        final String msg = format("Error when %s webhooks for repository %s",
                adding ? "adding" : "removing",
                repository.getRepositoryUrl());
        log.info(msg, e);
    }

    @PostConstruct
    public void init() throws Exception {
        repositoryDeletionExecutor = executorFactory.createRepositoryDeletionThreadPoolExecutor();
        webhookRemovalExecutor = executorFactory.createWebhookCleanupThreadPoolExecutor();
    }

    @PreDestroy
    public void destroy() throws Exception {
        executorFactory.shutdownExecutor(getClass().getName(), repositoryDeletionExecutor);
        executorFactory.shutdownExecutor(getClass().getName(), webhookRemovalExecutor);
    }

    @Override
    public List<Repository> getAllByOrganization(final int organizationId) {
        return repositoryDao.getAllByOrganization(organizationId, false);
    }

    @Override
    public List<Repository> getAllByOrganization(final int organizationId, final boolean includeDeleted) {
        return repositoryDao.getAllByOrganization(organizationId, includeDeleted);
    }

    @Override
    public Repository getByNameForOrganization(final int organizationId, final String repositoryName) {
        return repositoryDao.getByNameForOrganization(organizationId, repositoryName);
    }

    @Override
    public Repository get(final int repositoryId) {
        return repositoryDao.get(repositoryId);
    }

    @Override
    public Repository save(final Repository repository) {
        return repositoryDao.save(repository);
    }

    @Override
    public void syncRepositoryList(final Organization organization, final boolean soft) {
        if (!isOrganizationApproved(organization)) {
            logRepoListSyncAttemptedOnNonApprovedOrganization(organization.getId());
            return;
        }

        final Lock lock = clusterLockService.getLockForName(SYNC_REPOSITORY_LIST_LOCK);
        lock.lock();
        try {
            log.debug("Synchronizing list of repositories");

            final InvalidOrganizationManager invalidOrganizationsManager = new InvalidOrganizationsManagerImpl(pluginSettingsFactory);
            invalidOrganizationsManager.setOrganizationValid(organization.getId(), true);

            // get repositories from the dvcs hosting server
            final DvcsCommunicator communicator = communicatorProvider.getCommunicator(organization.getDvcsType());

            // get local repositories
            final List<Repository> storedRepositories = repositoryDao.getAllByOrganization(organization.getId(), true);

            List<Repository> remoteRepositories;
            try {
                remoteRepositories = communicator.getRepositories(organization, storedRepositories);
            } catch (SourceControlException.UnauthorisedException e) {
                // we could not load repositories, we can't continue
                // mark the organization as invalid
                invalidOrganizationsManager.setOrganizationValid(organization.getId(), false);
                throw e;
            }

            logRemoteRepositoriesReceived(remoteRepositories);

            // BBC-231 somehow we ended up with duplicated repositories on QA-EACJ
            removeDuplicateRepositories(organization, storedRepositories);
            // update names of existing repositories in case their names changed
            updateExistingRepositories(storedRepositories, remoteRepositories);
            // repositories that are no longer on hosting server will be marked as deleted
            removeDeletedRepositories(storedRepositories, remoteRepositories);
            // new repositories will be added to the database
            final Set<String> newRepoSlugs = addNewReposReturnNewSlugs(storedRepositories, remoteRepositories, organization);

            analyticsService.publishRepositoryListUpdated(organization, newRepoSlugs.size(), remoteRepositories.size());

            // start asynchronous changesets synchronization for all linked repositories in organization
            final EnumSet<SynchronizationFlag> synchronizationFlags = EnumSet.of(SYNC_CHANGESETS, SYNC_PULL_REQUESTS);
            if (soft) {
                synchronizationFlags.add(SynchronizationFlag.SOFT_SYNC);
            }
            syncAllInOrganization(organization.getId(), synchronizationFlags, newRepoSlugs);

        } finally {
            lock.unlock();
        }
    }

    private void logRemoteRepositoriesReceived(final List<Repository> remoteRepositories) {
        if (log.isDebugEnabled()) {
            try {
                final StringBuilder sb = new StringBuilder();
                sb.append("Received following repositories from server: [");
                remoteRepositories.forEach(r -> sb.append(r.getName()).append(", "));
                if (remoteRepositories.size() > 0) {
                    sb.delete(sb.length() - 2, sb.length());
                }
                sb.append("]");
                log.debug(sb.toString());
            } catch (Exception e) {
                // Doing nothing as we don't want to disturb syncing process due to logging issues
            }
        }
    }

    @Override
    public void syncRepositoryList(final Organization organization) {
        if (!isOrganizationApproved(organization)) {
            logRepoListSyncAttemptedOnNonApprovedOrganization(organization.getId());
            return;
        }

        syncRepositoryList(organization, true);
    }

    /**
     * Removes any repositories with duplicate slugs.
     *
     * @param organization       the owning organisation
     * @param storedRepositories the repositories from which to remove the duplicates
     */
    private void removeDuplicateRepositories(final Organization organization, final List<Repository> storedRepositories) {
        final Set<String> existingRepositories = new HashSet<>();
        for (final Repository repository : storedRepositories) {
            final String slug = repository.getSlug();
            if (existingRepositories.contains(slug)) {
                log.warn("Repository " + organization.getName() + "/" + slug + " is duplicated. Will be deleted.");
                remove(repository);
            } else {
                existingRepositories.add(slug);
            }
        }
    }

    /**
     * Adds the new repositories.
     *
     * @param storedRepositories the stored repositories
     * @param remoteRepositories the remote repositories
     * @param organization       the organization
     */
    private Set<String> addNewReposReturnNewSlugs(
            final List<Repository> storedRepositories,
            final List<Repository> remoteRepositories,
            final Organization organization) {
        final Set<String> newRepoSlugs = new HashSet<>();
        final Map<String, Repository> remoteRepos = makeRepositoryMap(remoteRepositories);

        // remove existing
        for (final Repository localRepo : storedRepositories) {
            remoteRepos.remove(localRepo.getSlug());
        }

        for (final Repository repository : remoteRepos.values()) {
            // save brand new
            repository.setOrganizationId(organization.getId());
            repository.setDvcsType(organization.getDvcsType());
            repository.setLinked(organization.isAutolinkNewRepos());
            repository.setCredential(organization.getCredential());
            repository.setSmartcommitsEnabled(organization.isSmartcommitsOnNewRepos());

            // need for installing post commit hook
            repository.setOrgHostUrl(organization.getHostUrl());
            repository.setOrgName(organization.getName());
            repository.setUpdateLinkAuthorised(true);

            final Repository savedRepository = repositoryDao.save(repository);
            newRepoSlugs.add(savedRepository.getSlug());
            log.debug("Adding new repository with name " + savedRepository.getName());
        }

        return newRepoSlugs;
    }

    private void updateAdminPermission(final Repository repository, final boolean hasAdminPermission) {
        if (repository.isLinked()) {
            Progress progress = repository.getSync();
            if (progress == null) {
                progress = new DefaultProgress();
                progress.setFinished(true);
                synchronizer.putProgress(repository, progress);
            }

            progress.setAdminPermission(hasAdminPermission);
        }
    }

    /**
     * Removes the deleted repositories.
     *
     * @param storedRepositories the stored repositories
     * @param remoteRepositories the remote repositories
     */
    private void removeDeletedRepositories(
            final List<Repository> storedRepositories, final List<Repository> remoteRepositories) {
        final Map<String, Repository> remoteRepos = makeRepositoryMap(remoteRepositories);
        for (final Repository localRepo : storedRepositories) {
            final Repository remoteRepo = remoteRepos.get(localRepo.getSlug());
            // does the remote repo exist?
            if (remoteRepo == null) {
                log.debug("Deleting repository " + localRepo);
                localRepo.setDeleted(true);
                repositoryDao.save(localRepo);
            }
        }
    }

    /**
     * Updates existing repositories - undelete existing deleted - updates names.
     *
     * @param storedRepositories the stored repositories
     * @param remoteRepositories the remote repositories
     */
    private void updateExistingRepositories(
            final List<Repository> storedRepositories, final List<Repository> remoteRepositories) {
        final Map<String, Repository> remoteRepos = makeRepositoryMap(remoteRepositories);
        for (final Repository localRepo : storedRepositories) {
            final Repository remoteRepo = remoteRepos.get(localRepo.getSlug());
            if (remoteRepo != null) {
                // set the name and save
                localRepo.setName(remoteRepo.getName());
                localRepo.setDeleted(false); // it could be deleted before and
                // now will be revived
                localRepo.setLogo(remoteRepo.getLogo());
                localRepo.setFork(remoteRepo.isFork());
                localRepo.setForkOf(remoteRepo.getForkOf());
                log.debug("Updating repository [{}]", localRepo);
                repositoryDao.save(localRepo);
            }
        }
    }

    /**
     * Converts collection of repository objects into map where key is repository slug and value is repository object.
     *
     * @param repositories the repositories
     * @return the map< string, repository>
     */
    private Map<String, Repository> makeRepositoryMap(final Collection<Repository> repositories) {
        final Map<String, Repository> map = Maps.newHashMap();
        for (final Repository repository : repositories) {
            map.put(repository.getSlug(), repository);
        }
        return map;
    }

    @Override
    @Nonnull
    public Optional<Progress> sync(final int repositoryId, final EnumSet<SynchronizationFlag> flags) {
        final Repository repository = get(repositoryId);

        // looks like repository was deleted before we started to synchronise it
        if (repository != null && !repository.isDeleted()) {
            if (!isOrganizationApproved(repository.getOrganizationId())) {
                logSyncAttemptedOnNonApprovedOrganization(repository.getOrganizationId());
                return Optional.empty();
            }
            return Optional.of(doSync(repository, flags));
        } else {
            log.warn("Sync requested but repository with id {} does not exist anymore.", repositoryId);
            return Optional.empty();
        }
    }

    /**
     * synchronization of changesets in all repositories which are in given organization
     *
     * @param organizationId organizationId
     * @param flags          flags
     * @param newRepoSlugs   newRepoSlugs
     */
    private void syncAllInOrganization(
            final int organizationId, final EnumSet<SynchronizationFlag> flags, final Set<String> newRepoSlugs) {
        if (!isOrganizationApproved(organizationId)) {
            logSyncAttemptedOnNonApprovedOrganization(organizationId);
            return;
        }

        final List<Repository> repositories = getAllByOrganization(organizationId);
        for (Repository repository : repositories) {
            if (!newRepoSlugs.contains(repository.getSlug())) {
                // not a new repo
                try {
                    doSync(repository, flags);
                } catch (SourceControlException.SynchronizationDisabled e) {
                    // ignoring
                }
            } else {
                // it is a new repo, we force to hard sync
                // to disable smart commits on it, make sense
                // in case when someone has just migrated
                // repo to DVCS avoiding duplicate smart commits
                EnumSet<SynchronizationFlag> newFlags = EnumSet.copyOf(flags);
                newFlags.remove(SynchronizationFlag.SOFT_SYNC);

                try {
                    doSync(repository, newFlags);
                } catch (SourceControlException.SynchronizationDisabled e) {
                    // ignoring
                }

                if (repository.isLinked()) {
                    try {
                        addOrRemovePostcommitHook(repository, getPostCommitUrl(repository));
                    } catch (SourceControlException.PostCommitHookRegistrationException e) {
                        // if the user didn't have rights to add post commit hook, just unlink the repository
                        logFailedWebhook(repository, true, e);
                        updateAdminPermission(repository, false);
                        repositoryDao.save(repository);
                    } catch (Exception e) {
                        // we could not add hooks for some reason, let's leave the repository unlinked
                        logFailedWebhook(repository, true, e);
                        repositoryDao.save(repository);
                    }
                }
            }
        }
    }

    /**
     * Do sync.
     *
     * @param repository the repository
     * @param flags      the flags
     * @return the progress object for the sync
     */
    private Progress doSync(final Repository repository, final EnumSet<SynchronizationFlag> flags) {
        synchronizer.doSync(repository, flags);
        return synchronizer.getProgress(repository.getId());
    }

    @Override
    public List<Repository> getAllRepositories() {
        return repositoryDao.getAll(false);
    }

    @Override
    public List<Repository> getAllRepositories(final boolean includeDeleted) {
        return repositoryDao.getAll(includeDeleted);
    }

    @Override
    public List<Repository> getAllRepositories(final String dvcsType, final boolean includeDeleted) {
        return repositoryDao.getAllByType(dvcsType, includeDeleted);
    }

    @Override
    public boolean existsLinkedRepositories() {
        return repositoryDao.existsLinkedRepositories(false);
    }

    @Override
    public RepositoryRegistration enableRepository(final int repoId, final boolean linked) {
        final RepositoryRegistration registration = new RepositoryRegistration();
        final Repository repository = repositoryDao.get(repoId);

        if (repository != null) {
            registration.setRepository(repository);

            // un/pause possible synchronization
            synchronizer.pauseSynchronization(repository, !linked);

            repository.setLinked(linked);
            setSmartCommitsToCurrentOrganizationState(repository);
            final String postCommitUrl = getPostCommitUrl(repository);
            registration.setCallBackUrl(postCommitUrl);

            try {
                addOrRemovePostcommitHook(repository, postCommitUrl);
                registration.setCallBackUrlInstalled(linked);
                updateAdminPermission(repository, true);
            } catch (SourceControlException.PostCommitHookRegistrationException e) {
                logFailedWebhook(repository, linked, e);
                registration.setCallBackUrlInstalled(!linked);
                updateAdminPermission(repository, false);
            } catch (Exception e) {
                logFailedWebhook(repository, linked, e);
                registration.setCallBackUrlInstalled(!linked);
            }

            log.debug("Enable repository [{}]", repository);
            repositoryDao.save(repository);

            try {
                linkerService.updateConnectLinkerValuesAsync(repository.getOrganizationId());
            } catch (RejectedExecutionException e) {
                log.warn("Failed to schedule task to cleanup old linker values for Organization {}, this will " +
                        "require manual cleanup", repository.getOrganizationId());
            }
        }

        return registration;
    }

    private void setSmartCommitsToCurrentOrganizationState(Repository repository) {
        repository.setSmartcommitsEnabled(organizationDao.get(repository.getOrganizationId()).isSmartcommitsOnNewRepos());
    }

    @Override
    public void enableRepositorySmartcommits(final int repoId, final boolean enabled) {
        Repository repository = repositoryDao.get(repoId);
        if (repository != null) {
            repository.setSmartcommitsEnabled(enabled);
            smartCommitsAnalyticsService.fireSmartCommitPerRepoConfigChange(repoId, enabled);
            log.debug("Enable repository smartcommits [{}]", repository);
            repositoryDao.save(repository);
        }
    }

    /**
     * Adds the or remove postcommit hook.
     * <p>
     * For use with non-Principal based repositories only.
     *
     * @param repository            the repository
     * @param postCommitCallbackUrl commit callback url
     */
    private void addOrRemovePostcommitHook(@Nonnull final Repository repository, final String postCommitCallbackUrl) {
        requireNonNull(repository, "A repository is required");

        if (!shouldRemovePostcommitHooks(repository.getCredential(), false)) {
            log.debug("Principal-based repository provided; Not modifying linkers or webhooks.");
            return;
        }

        final DvcsCommunicator communicator = communicatorProvider.getCommunicator(repository.getDvcsType());

        if (repository.isLinked()) {
            communicator.ensureHookPresent(repository, postCommitCallbackUrl);
            // TODO: move linkRepository to setupPostcommitHook if possible
            communicator.linkRepository(repository, changesetService.findReferencedProjects(repository.getId()));
        } else {
            communicator.removePostcommitHook(repository, postCommitCallbackUrl);
        }
    }

    @Override
    public boolean removePostcommitHook(@Nonnull final Repository repository, final boolean ignoreCredentialType) {
        requireNonNull(repository, "A repository is required");

        if (!shouldRemovePostcommitHooks(repository.getCredential(), ignoreCredentialType)) {
            log.debug("Principal-based repository provided; Not modifying webhooks.");
            return false;
        }

        log.debug("Removing postcommit hooks for repository {} ({})", repository.getId(), repository.getRepositoryUrl());
        try {
            final DvcsCommunicator communicator = communicatorProvider.getCommunicator(repository.getDvcsType());
            communicator.removePostcommitHook(repository, getPostCommitUrl(repository));
            return true;
        } catch (Exception e) {
            log.info(format("Failed to uninstall postcommit hook for repository id %d (%s)",
                    repository.getId(), repository.getRepositoryUrl()), e);
            return false;
        }
    }

    @Override
    public int removePostcommitHooks(@Nonnull final Organization organization, final boolean ignoreCredentialType) {
        requireNonNull(organization, "An organization is required");

        if (!shouldRemovePostcommitHooks(organization.getCredential(), ignoreCredentialType)) {
            log.debug("Principal-based organization provided; Not modifying webhooks.");
            return 0;
        }

        int removedCount = 0;
        for (Repository r : getAllByOrganization(organization.getId())) {
            if (removePostcommitHook(r, ignoreCredentialType)) {
                removedCount++;
            }
        }
        return removedCount;
    }

    @Override
    @Nonnull
    public Future<Integer> removePostcommitHooksAsync(@Nonnull final Organization organization,
                                                      final boolean ignoreCredentialType) {
        requireNonNull(organization, "An organization is required");

        if (!shouldRemovePostcommitHooks(organization.getCredential(), ignoreCredentialType)) {
            log.debug("Principal-based organization provided; Not modifying webhooks.");
            return CompletableFuture.completedFuture(0);
        }

        return webhookRemovalExecutor.submit(() -> removePostcommitHooks(organization, ignoreCredentialType));
    }

    /**
     * Postcommit hooks should only be removed for non-Principal based orgs/repos, unless we are explicitly forcing
     * the removal (as in the case of cleaning up hooks on migration of an org to a Principal-based credential).
     *
     * @param credential           The credential of the Organization or Repository
     * @param ignoreCredentialType Whether to force the removal
     * @return <code>true</code> if the remove should occur; <code>false</code> otherwise.
     */
    private boolean shouldRemovePostcommitHooks(@Nullable final Credential credential, final boolean ignoreCredentialType) {
        return ignoreCredentialType ||
                credential == null ||
                !credential.accept(PrincipalIDCredential.visitor()).isPresent();
    }

    /**
     * Gets the post commit url.
     *
     * @param repo the repo
     * @return the post commit url
     */
    private String getPostCommitUrl(final Repository repo) {
        return applicationProperties.getBaseUrl() + POST_HOOK_SUFFIX + repo.getId() + "/sync";
    }

    @Override
    public void removeRepositories(final List<Repository> repositories) {
        log.debug("Removing {} repositories.", repositories.size());
        for (final Repository repository : repositories) {
            log.debug("Removing repository {}.", repository.getId());
            if (!repository.isDeleted()) {
                log.trace("Marking repository {} as deleted.", repository.getId());
                repository.setDeleted(true);
                repositoryDao.save(repository);
                synchronizer.pauseSynchronization(repository, true);
                log.trace("Repository {} marked as deleted and synchronization stopped.", repository.getId());
            }

            // try remove postcommit hook
            if (shouldRemoveLinkersAndCommitHooks(repository)) {
                log.trace("Removing linkers and webhooks for repository {}.", repository.getId());
                removePostcommitHook(repository, false);
                repository.setLinked(false);
                repositoryDao.save(repository);
                log.trace("Linkers and webhooks removed for repository {}.", repository.getId());
            }
        }
    }

    @VisibleForTesting
    boolean shouldRemoveLinkersAndCommitHooks(@Nonnull final Repository repository) {
        final Option<PrincipalIDCredential> principalIDCredentialOption =
                fromOptional(repository.getCredential().accept(PrincipalIDCredential.visitor()));
        final Option<ACIInstallationService> aciInstallationServiceOption =
                fromOptional(aciInstallationServiceAccessor.get());

        if (principalIDCredentialOption.isDefined() && aciInstallationServiceOption.isDefined()) {
            // if this repository is managed by ACI we only want to try and remove the linkers and commit hooks
            // if the installation is not in the UNINSTALLED state, since otherwise we won't be able to create
            // valid JWTs to sign the requests to perform the removals
            Option<Installation> installationOption = principalIDCredentialOption.flatMap(principalIdCredential ->
                    aciInstallationServiceOption.flatMap(aciInstallationService -> {
                        try {
                            Installation installation = aciInstallationService.get(BITBUCKET_CONNECTOR_APPLICATION_ID,
                                    principalIdCredential.getPrincipalId());
                            if (!installation.getLifeCycleStage().equals(Installation.LifeCycleStage.UNINSTALLED)) {
                                return Option.some(installation);
                            } else {
                                return Option.none();
                            }
                        } catch (Exception e) {
                            log.debug("retrieving ACI installation failed, will return none", e);
                            return Option.none();
                        }
                    }));
            return repository.isLinked() && installationOption.isDefined();
        }

        return repository.isLinked();
    }

    @Override
    public void remove(final Repository repository, final boolean shouldRemoveLinkersAndCommitHooks) {
        final long startTime = System.currentTimeMillis();
        synchronizer.stopSynchronization(repository);
        eventService.discardEvents(repository);

        // try remove postcommit hook
        if (shouldRemoveLinkersAndCommitHooks) {
            removePostcommitHook(repository, false);
        }
        // remove all changesets from DB that references this repository
        changesetService.removeAllInRepository(repository.getId());
        // remove progress
        synchronizer.removeProgress(repository);
        // delete branches saved for repository
        branchService.removeAllBranchHeadsInRepository(repository.getId());
        branchService.removeAllBranchesInRepository(repository.getId());
        // remove pull requests things
        gitHubEventService.removeAll(repository);
        repositoryPullRequestDao.removeAll(repository);
        // remove sync logs
        syncAuditDao.removeAllForRepo(repository.getId());
        // delete repository record itself
        repositoryDao.remove(repository.getId());
        log.debug("Repository {} was deleted in {} ms", repository.getId(), System.currentTimeMillis() - startTime);
    }

    @Override
    public void remove(final Repository repository) {
        remove(repository, shouldRemoveLinkersAndCommitHooks(repository));
    }

    @Override
    public void removeOrphanRepositories(final List<Repository> orphanRepositories) {
        // submit as multiple tasks so that we could quickly terminate the executor
        log.debug("Starting to remove {} orphan repositories", orphanRepositories.size());
        for (final Repository orphanRepository : orphanRepositories) {
            repositoryDeletionExecutor.execute(() -> {
                try {
                    remove(orphanRepository, shouldRemoveLinkersAndCommitHooks(orphanRepository));
                    log.debug("Removed orphan repository {}", orphanRepository);
                } catch (Exception e) {
                    log.info("Unexpected exception when removing orphan repository " + orphanRepository, e);
                }
            });
        }
    }

    @Override
    public void onOffLinkers(final boolean enableLinkers) {
        log.debug("Enable linkers : " + BooleanUtils.toStringYesNo(enableLinkers));

        // remove the variable first so adding and removing linkers works
        pluginSettingsFactory.createGlobalSettings().remove(LINKERS_ENABLED_SETTINGS_PARAM);

        // add or remove linkers
        for (final Repository repository : getAllRepositories()) {
            log.debug((enableLinkers ? "Adding" : "Removing") + " linkers for" + repository.getSlug());

            final DvcsCommunicator communicator = communicatorProvider.getCommunicator(repository.getDvcsType());
            if (enableLinkers && repository.isLinked()) {
                communicator.linkRepository(repository, changesetService.findReferencedProjects(repository.getId()));
            } else {
                communicator.linkRepository(repository, new HashSet<>());
            }
        }

        if (!enableLinkers) {
            pluginSettingsFactory.createGlobalSettings()
                    .put(LINKERS_ENABLED_SETTINGS_PARAM, Boolean.FALSE.toString());
        }
    }

    @Override
    public DvcsUser getUser(final Repository repository, final String author, final String rawAuthor) {
        log.debug("Get user information for: [ {}, {}]", author, rawAuthor);
        final DvcsCommunicator communicator = communicatorProvider.getCommunicator(repository.getDvcsType());

        DvcsUser user = null;

        if (!isNullOrEmpty(author)) {
            try {
                user = communicator.getUser(repository, author);
            } catch (Exception e) {
                log.info(format("Could not load user [%s, %s]", author, rawAuthor), e);
                return getUnknownUser(repository, author, rawAuthor);
            }
        }

        return user != null ? user : getUnknownUser(repository, author, rawAuthor);
    }

    public void setPreviouslyLinkedProjects(final Repository repository, final Set<String> projects) {
        repositoryDao.setPreviouslyLinkedProjects(repository.getId(), projects);
    }

    public List<String> getPreviouslyLinkedProjects(final Repository repository) {
        return repositoryDao.getPreviouslyLinkedProjects(repository.getId());
    }

    @Override
    public Set<String> getEmails(final Repository repository, final DvcsUser user) {
        return changesetService.findEmails(repository.getId(), user.getUsername());
    }

    @Override
    public int disableAllRepositories() {
        final Collection<Repository> enabledRepos =
                getAllRepositories().stream().filter(Repository::isLinked).collect(toList());
        enabledRepos.stream().forEach(this::disable);
        return enabledRepos.size();
    }

    private void disable(final Repository repository) {
        enableRepository(repository.getId(), false);
    }

    /**
     * Creates user, which is unknown - it means he does not exist as real user inside a repository. But we still want
     * to provide some information about him.
     *
     * @param repository system, which should know, who is the provided user
     * @param username   of user or null/empty string if does not exist
     * @param rawUser    DVCS representation of user, for git/mercurial is it: <i>Full Name &lt;email&gt;</i>
     * @return "unknown" user
     */
    private UnknownUser getUnknownUser(final Repository repository, final String username, final String rawUser) {
        return new UnknownUser(username, rawUser != null ? rawUser : username, repository.getOrgHostUrl());
    }

    private boolean isOrganizationApproved(final Organization org) {
        return org.getApprovalState() == Organization.ApprovalState.APPROVED;
    }

    private boolean isOrganizationApproved(final int orgId) {
        final Organization org = organizationDao.get(orgId);
        if (org == null) {
            throw new IllegalArgumentException("Cannot find Organization with id: " + orgId);
        }
        return isOrganizationApproved(org);
    }

    private void logRepoListSyncAttemptedOnNonApprovedOrganization(final int orgId) {
        log.debug("Attempted to trigger Repository List sync on Organization that "
                + "has not been approved. orgId: {}", orgId);
    }

    private void logSyncAttemptedOnNonApprovedOrganization(final int orgId) {
        log.debug("Attempted to trigger sync on Repository belonging to Organization that "
                + "has not been approved. orgId: {}", orgId);
    }
}
