package it.restart.com.atlassian.jira.plugins.dvcs.test;

import com.atlassian.jira.plugins.dvcs.model.Changeset;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.pageobjects.page.AccountType;
import com.atlassian.jira.plugins.dvcs.rest.DevToolsClient;
import com.atlassian.jira.plugins.dvcs.rest.OrganizationClient;
import com.atlassian.jira.webtests.util.JIRAEnvironmentData;
import it.util.TestAccounts;

import java.util.List;

import static org.fest.assertions.api.Assertions.assertThat;

public class DVCSTestHelper {

    private final OrganizationClient organizationClient;
    private final DevToolsClient devToolsClient;

    public DVCSTestHelper(final JIRAEnvironmentData jiraEnvironmentData) {
        organizationClient = new OrganizationClient(jiraEnvironmentData);
        devToolsClient = new DevToolsClient(jiraEnvironmentData);
    }

    public void clearAllOrganizations() {
        organizationClient.deleteAll();
    }

    public Organization addApprovedBitbucketAccount() {
        return organizationClient.addOrganizationForApprovedAccount(TestAccounts.JIRA_BB_CONNECTOR_ACCOUNT, AccountType.BITBUCKET.type,
                TestAccounts.JIRA_BB_CONNECTOR_KEY, TestAccounts.JIRA_BB_CONNECTOR_SECRET, TestAccounts.BB_URL, false, false);
    }

    public void assertCommits(final String issueKey, final int expectedCommits, final String expectedMessage) {
        final List<Changeset> commits = devToolsClient.getCommitDetails(issueKey);
        assertThat(commits).hasSize(expectedCommits);
        for (final Changeset commit : commits) {
            if (expectedMessage.equals(commit.getMessage())) {
                return;
            }
        }
        throw new AssertionError("No commit with message '" + expectedMessage + "'");
    }

}
