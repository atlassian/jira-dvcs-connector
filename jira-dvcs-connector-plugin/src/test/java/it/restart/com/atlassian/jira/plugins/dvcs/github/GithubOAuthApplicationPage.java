package it.restart.com.atlassian.jira.plugins.dvcs.github;

import com.atlassian.jira.plugins.dvcs.pageobjects.common.OAuth;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static it.restart.com.atlassian.jira.plugins.dvcs.test.GithubTestHelper.GITHUB_URL;

public class GithubOAuthApplicationPage implements Page {
    private static final Logger LOG = LoggerFactory.getLogger(GithubOAuthApplicationPage.class);
    private final String hostUrl;
    @Inject
    private PageElementFinder pageElementFinder;
    @Inject
    private PageBinder pageBinder;

    public GithubOAuthApplicationPage() {
        this(GITHUB_URL);
    }

    public GithubOAuthApplicationPage(String hostUrl) {
        this.hostUrl = hostUrl;
    }

    private static void removeConsumer(final GithubOAuthApplicationPage page, final By bySelector) {
        final PageElement link = page.pageElementFinder.find(bySelector);
        if (link.isPresent() && link.isVisible()) {
            link.click();
            page.pageBinder.bind(GithubOAuthPage.class).removeConsumer();
        } else {
            // get next link
            final PageElement nextPageLink = page.pageElementFinder.find(By.className("next_page"));
            if (nextPageLink.isPresent() && nextPageLink.isVisible() && nextPageLink.getTagName().equalsIgnoreCase("a")
                    && nextPageLink.isEnabled()) {
                nextPageLink.click();
                waitUntilTrue(page.pageElementFinder.find(By.className("table-list-bordered")).timed().isPresent());
                final GithubOAuthApplicationPage nextPage = page.pageBinder.bind(GithubOAuthApplicationPage.class);
                removeConsumer(nextPage, bySelector);
            } else {
                LOG.error("Can not find Github consumer application link using selector {}", bySelector);
            }
        }
    }

    @Override
    public String getUrl() {
        return hostUrl + "/settings/developers";
    }

    public void removeConsumer(final OAuth oAuth) {
        this.removeConsumer(StringUtils.removeStart(oAuth.applicationId, hostUrl));
    }

    public void removeConsumer(final String appUri) {
        removeConsumer(By.xpath("//a[@href='" + appUri + "']"));
    }

    public void removeConsumerForAppName(final String appName) {
        removeConsumer(By.linkText(appName));
    }

    public void removeConsumer(By bySelector) {
        removeConsumer(this, bySelector);
    }

    public List<PageElement> findOAthApplications(final String partialLinkText) {
        return pageElementFinder.findAll(By.partialLinkText(partialLinkText));
    }
}
