package com.atlassian.jira.plugins.dvcs.dao.impl.transform;

import com.atlassian.jira.plugins.dvcs.activeobjects.v3.OrganizationMapping;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.RepositoryMapping;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialType;
import com.atlassian.jira.plugins.dvcs.model.credential.PrincipalIDCredential;
import com.atlassian.jira.plugins.dvcs.model.credential.UnauthenticatedCredential;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Instant;
import java.util.Date;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class RepositoryTransformerTest {
    private static final int REPO_ID = 123;
    private static final String REPO_NAME = "repo";
    private static final String REPO_SLUG = "repo_slug";

    private static final String FORK_NAME = "fork";
    private static final String FORK_OWNER = "fork_owner";
    private static final String FORK_SLUG = "fork_slug";

    private static final Date LAST_SYNC_DATE = Date.from(Instant.parse("2015-09-01T10:00:00.00Z"));
    private static final Date LAST_COMMIT_DATE = Date.from(Instant.parse("2015-09-01T11:00:00.00Z"));

    private static final Boolean UPDATE_AUTHORIZED = Boolean.TRUE;
    private static final Boolean SMART_COMMITS = Boolean.TRUE;
    private static final Boolean LINKED = Boolean.TRUE;
    private static final Boolean DELETED = Boolean.FALSE;

    private static final int ORG_ID = 456;
    private static final String ORG_NAME = "org";
    private static final String HOST_URL = "https://bitbucket.org";
    private static final String DVCS_TYPE = "bitbucket";
    private static final String OAUTH_KEY = "key";
    private static final String OAUTH_SECRET = "secret";
    private static final String ACCESS_TOKEN = "token";
    private static final String PRINCIPAL_ID = "principal";
    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";

    private RepositoryTransformer repositoryTransformer;

    @Mock
    private RepositoryMapping repositoryMapping;

    @Mock
    private OrganizationMapping organizationMapping;

    @BeforeMethod
    public void setup() {
        initMocks(this);

        repositoryTransformer = new RepositoryTransformer();

        setupRepositoryMapping();
        setupOrganizationMapping();
    }

    private void setupRepositoryMapping() {
        when(repositoryMapping.getID()).thenReturn(REPO_ID);
        when(repositoryMapping.getName()).thenReturn(REPO_NAME);
        when(repositoryMapping.getSlug()).thenReturn(REPO_SLUG);
        when(repositoryMapping.getOrganizationId()).thenReturn(ORG_ID);

        when(repositoryMapping.isFork()).thenReturn(true);
        when(repositoryMapping.getForkOfName()).thenReturn(FORK_NAME);
        when(repositoryMapping.getForkOfOwner()).thenReturn(FORK_OWNER);
        when(repositoryMapping.getForkOfSlug()).thenReturn(FORK_SLUG);

        when(repositoryMapping.getActivityLastSync()).thenReturn(LAST_SYNC_DATE);
        when(repositoryMapping.getLastCommitDate()).thenReturn(LAST_COMMIT_DATE);

        when(repositoryMapping.getUpdateLinkAuthorised()).thenReturn(UPDATE_AUTHORIZED);
        when(repositoryMapping.isSmartcommitsEnabled()).thenReturn(SMART_COMMITS);
        when(repositoryMapping.isLinked()).thenReturn(LINKED);
        when(repositoryMapping.isDeleted()).thenReturn(DELETED);
    }

    private void setupOrganizationMapping() {
        when(organizationMapping.getID()).thenReturn(ORG_ID);
        when(organizationMapping.getName()).thenReturn(ORG_NAME);
        when(organizationMapping.getDvcsType()).thenReturn(DVCS_TYPE);
        when(organizationMapping.getHostUrl()).thenReturn(HOST_URL);
        when(organizationMapping.getOauthKey()).thenReturn(OAUTH_KEY);
        when(organizationMapping.getOauthSecret()).thenReturn(OAUTH_SECRET);
        when(organizationMapping.getAccessToken()).thenReturn(ACCESS_TOKEN);
        when(organizationMapping.getPrincipalId()).thenReturn(PRINCIPAL_ID);
        when(organizationMapping.getAdminUsername()).thenReturn(USERNAME);
        when(organizationMapping.getAdminPassword()).thenReturn(PASSWORD);
    }

    @Test
    public void testWithNullRepo() {
        assertThat(repositoryTransformer.transform(null, null, null), nullValue());
    }

    @Test
    public void testWithNullOrgAndProgress() {

        final Repository result = repositoryTransformer.transform(repositoryMapping, null, null);

        assertThat(result, notNullValue());
        assertThat(result.getName(), equalTo(REPO_NAME));
        assertThat(result.getSlug(), equalTo(REPO_SLUG));
        assertThat(result.getActivityLastSync(), equalTo(LAST_SYNC_DATE));
        assertThat(result.getLastCommitDate(), equalTo(LAST_COMMIT_DATE));

        assertThat(result.getForkOf(), notNullValue());
        assertThat(result.getForkOf().getName(), equalTo(FORK_NAME));
        assertThat(result.getForkOf().getOwner(), equalTo(FORK_OWNER));
        assertThat(result.getForkOf().getSlug(), equalTo(FORK_SLUG));
        assertThat(result.getForkOf().getOrganizationId(), equalTo(0));

        assertThat(result.getOrganizationId(), equalTo(ORG_ID));
        assertThat(result.getDvcsType(), nullValue());
        assertThat(result.getCredential(), equalTo(new UnauthenticatedCredential()));
        assertThat(result.getOrgHostUrl(), nullValue());
        assertThat(result.getRepositoryUrl(), nullValue());

        assertThat(result.getSync(), nullValue());
    }

    @Test
    public void testWithNonNullOrg() {

        final Repository result = repositoryTransformer.transform(repositoryMapping, organizationMapping, null);

        assertThat(result, notNullValue());
        assertThat(result.getName(), equalTo(REPO_NAME));
        assertThat(result.getSlug(), equalTo(REPO_SLUG));
        assertThat(result.getActivityLastSync(), equalTo(LAST_SYNC_DATE));
        assertThat(result.getLastCommitDate(), equalTo(LAST_COMMIT_DATE));

        assertThat(result.getForkOf(), notNullValue());
        assertThat(result.getForkOf().getName(), equalTo(FORK_NAME));
        assertThat(result.getForkOf().getOwner(), equalTo(FORK_OWNER));
        assertThat(result.getForkOf().getSlug(), equalTo(FORK_SLUG));
        assertThat(result.getForkOf().getOrganizationId(), equalTo(0));

        assertThat(result.getOrganizationId(), equalTo(ORG_ID));
        assertThat(result.getDvcsType(), equalTo(DVCS_TYPE));
        assertThat(result.getOrgHostUrl(), equalTo(HOST_URL));
        assertThat(result.getRepositoryUrl(), equalTo(HOST_URL + "/" + ORG_NAME + "/" + REPO_SLUG));

        assertThat(result.getCredential(), notNullValue());
        assertThat(result.getCredential().getType(), equalTo(CredentialType.PRINCIPAL_ID));
        assertThat(((PrincipalIDCredential) result.getCredential()).getPrincipalId(), equalTo(PRINCIPAL_ID));

        assertThat(result.getSync(), nullValue());
    }
}
