package com.atlassian.jira.plugins.dvcs.navlinks;

import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Organization.ApprovalState;
import com.atlassian.jira.plugins.dvcs.service.OrganizationService;
import com.atlassian.plugins.navlink.producer.navigation.services.RawNavigationLink;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;

import static com.atlassian.jira.plugins.dvcs.model.Organization.ApprovalState.APPROVED;
import static com.atlassian.jira.plugins.dvcs.model.Organization.ApprovalState.PENDING_APPROVAL;
import static com.atlassian.jira.plugins.dvcs.navlinks.BitbucketCloudAccountNavLinkRepository.BITBUCKET_ORG_TYPE;
import static com.atlassian.jira.plugins.dvcs.navlinks.BitbucketCloudAccountNavLinkRepository.getLinkLabel;
import static com.google.common.collect.Lists.newArrayList;
import static io.atlassian.fugue.Pair.zip;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@ParametersAreNonnullByDefault
public class BitbucketCloudAccountNavLinkRepositoryTest {

    /**
     * Creates a mock org using the given values.
     *
     * @param name the org name
     * @param approvalState the EasyConnect approval state
     * @return see above
     */
    @Nonnull
    private static Organization mockOrganization(final String name, final ApprovalState approvalState) {
        final Organization organization = mock(Organization.class);
        when(organization.getName()).thenReturn(name);
        when(organization.getApprovalState()).thenReturn(approvalState);
        when(organization.getOrganizationUrl()).thenReturn("nonNull");
        return organization;
    }

    @Mock
    private OrganizationService organizationService;

    @InjectMocks
    private BitbucketCloudAccountNavLinkRepository navLinkRepository;

    @Test
    public void shouldApplyGivenNavLinkPredicate() {
        // Set up
        final Organization org1 = setUpTwoOrganizations().get(0);
        final Predicate<RawNavigationLink> navLinkPredicate = input -> input.getLabelKey().contains("1");

        // Invoke
        final List<RawNavigationLink> navLinks = newArrayList(navLinkRepository.matching(navLinkPredicate));

        // Check
        assertThat(navLinks, hasSize(1));
        assertThat(navLinks.get(0).getLabelKey(), is(getLinkLabel(org1)));
    }

    @Test
    public void shouldTreatNullPredicateAsNoPredicate() {
        // Set up
        final List<Organization> orgs = setUpTwoOrganizations();

        // Invoke
        final List<RawNavigationLink> navLinks = newArrayList(navLinkRepository.matching(null));

        // Check
        assertThat(navLinks, hasSize(2));
        zip(navLinks, orgs).forEach(
            pair -> assertThat(pair.left().getLabelKey(), is(getLinkLabel(pair.right())))
        );
    }

    private List<Organization> setUpTwoOrganizations() {
        final Organization org1 = mockOrganization("myOrg1", APPROVED);
        final Organization org2 = mockOrganization("myOrg2", APPROVED);
        final Organization org3 = mockOrganization("myOrg3", PENDING_APPROVAL); // should never be linked
        final ImmutableList<Organization> orgs = ImmutableList.of(org1, org2, org3);
        when(organizationService.getAll(false, BITBUCKET_ORG_TYPE)).thenReturn(orgs);
        return orgs;
    }
}
