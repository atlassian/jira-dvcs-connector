package com.atlassian.jira.plugins.dvcs.webwork;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.plugins.dvcs.auth.OAuthStore;
import com.atlassian.jira.plugins.dvcs.bbrebrand.BitbucketRebrandDarkFeature;
import com.atlassian.jira.plugins.dvcs.featurediscovery.FeatureDiscoveryService;
import com.atlassian.jira.plugins.dvcs.listener.PluginFeatureDetector;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory;
import com.atlassian.jira.plugins.dvcs.service.InvalidOrganizationManager;
import com.atlassian.jira.plugins.dvcs.service.OrganizationService;
import com.atlassian.jira.plugins.dvcs.service.remote.SyncDisabledHelper;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.BitbucketCommunicator;
import com.atlassian.jira.plugins.dvcs.spi.github.GithubCommunicator;
import com.atlassian.jira.plugins.dvcs.spi.githubenterprise.GithubEnterpriseCommunicator;
import com.atlassian.jira.projects.unlicensed.UnlicensedProjectPageRenderer;
import com.atlassian.jira.software.api.conditions.SoftwareGlobalAdminCondition;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.List;

import static com.atlassian.jira.plugins.dvcs.webwork.ConfigureDvcsOrganizations.BITBUCKET_CLOUD_NAME;
import static com.atlassian.jira.plugins.dvcs.webwork.ConfigureDvcsOrganizations.GIT_HUB_ENTERPRISE_NAME;
import static com.atlassian.jira.plugins.dvcs.webwork.ConfigureDvcsOrganizations.GIT_HUB_NAME;
import static com.atlassian.jira.plugins.dvcs.webwork.ConfigureDvcsOrganizations.SYNCHRONIZATION_ALL_DISABLED_MESSAGE_KEY;
import static com.atlassian.jira.plugins.dvcs.webwork.ConfigureDvcsOrganizations.SYNCHRONIZATION_DISABLED_MESSAGE_KEY;
import static com.atlassian.jira.plugins.dvcs.webwork.ConfigureDvcsOrganizations.SYNCHRONIZATION_DISABLED_TITLE_KEY;
import static com.atlassian.jira.plugins.dvcs.webwork.ConfigureDvcsOrganizations.UNLICENSED;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static webwork.action.Action.INPUT;

public class ConfigureDvcsOrganizationsTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @InjectMocks
    private ConfigureDvcsOrganizations configureDvcsOrganizations;

    @Mock
    private BitbucketRebrandDarkFeature bitbucketRebrandDarkFeature;

    @Mock
    private EventPublisher eventPublisher;

    @Mock
    private FeatureManager featureManager;

    @Mock
    private I18nHelper i18nHelper;

    @Mock
    private InvalidOrganizationManager invalidOrganizationsManager;

    @Mock
    private OAuthStore oAuthStore;

    @Mock
    private OrganizationService organizationService;

    @Mock
    private PageBuilderService pageBuilderService;

    @Mock
    private PluginFeatureDetector pluginFeatureDetector;

    @Mock
    private PluginSettingsFactory pluginSettingsFactory;

    @Mock
    private SoftwareGlobalAdminCondition softwareGlobalAdminCondition;

    @Mock
    private SyncDisabledHelper syncDisabledHelper;

    @Mock
    private UnlicensedProjectPageRenderer unlicensedProjectPageRenderer;

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private FeatureDiscoveryService featureDiscoveryService;

    private static String i18nMessage(final String key, Object... params) {
        return key + Arrays.toString(params);
    }

    @Before
    public void setup() throws Exception {
        when(i18nHelper.getText(any())).then(i -> i.getArguments()[0]);
        when(i18nHelper.getText(any(), any())).then(i -> i18nMessage((String) i.getArguments()[0], i.getArguments()[1]));
        when(bitbucketRebrandDarkFeature.isBitbucketRebrandEnabled()).thenReturn(true);
    }

    @Test
    public void doDefault_shouldReturnUnlicensedView_whenSoftwareGlobalAdminConditionIsNotMet()
            throws Exception {
        when(softwareGlobalAdminCondition.shouldDisplay(anyMapOf(String.class, Object.class))).thenReturn(false);
        String result = configureDvcsOrganizations.doDefault();

        assertThat(result, is(UNLICENSED));
    }

    @Test
    public void doDefault_shouldReturnInputView_whenSoftwareGlobalAdminConditionIsMet() throws Exception {
        when(softwareGlobalAdminCondition.shouldDisplay(anyMapOf(String.class, Object.class))).thenReturn(true);
        String result = configureDvcsOrganizations.doDefault();

        assertThat(result, is(INPUT));
    }

    @Test
    public void loadOrganizations_returnsSortedList_whenMultipleOrgs() throws Exception {
        when(organizationService.getAll(eq(true))).thenReturn(asList(
                createOrganization("Org-E", false),
                createOrganization("Org-C", true),
                createOrganization("Org-B", true),
                createOrganization("Org-D", false),
                createOrganization("Org-A", false)
        ));

        final List<Organization> organizations = configureDvcsOrganizations.loadOrganizations();
        assertThat(organizations.size(), is(5));

        final List<String> orgNames = organizations.stream().map(Organization::getName).collect(toList());
        assertThat(orgNames, equalTo(asList("Org-B", "Org-C", "Org-A", "Org-D", "Org-E")));
    }

    @Test
    public void loadOrganizations_returnsEmptyList_whenNoOrgs() throws Exception {
        when(organizationService.getAll(eq(true))).thenReturn(emptyList());

        final List<Organization> organizations = configureDvcsOrganizations.loadOrganizations();
        assertThat(organizations.size(), is(0));
    }

    @Test
    public void isAnySyncDisabled_returnsTrue_whenBitbucketSyncDisabled() throws Exception {
        setSystemSyncDisabled(true, false, false);
        assertThat(configureDvcsOrganizations.isAnySyncDisabled(), is(true));
    }

    @Test
    public void isAnySyncDisabled_returnsTrue_whenGithubSyncDisabled() throws Exception {
        setSystemSyncDisabled(false, true, false);
        assertThat(configureDvcsOrganizations.isAnySyncDisabled(), is(true));
    }

    @Test
    public void isAnySyncDisabled_returnsTrue_whenGithubEnterpriseSyncDisabled() throws Exception {
        setSystemSyncDisabled(false, false, true);
        assertThat(configureDvcsOrganizations.isAnySyncDisabled(), is(true));
    }

    @Test
    public void isAnySyncDisabled_returnsFalse_whenAllSyncEnabled() throws Exception {
        setSystemSyncDisabled(false, false, false);
        assertThat(configureDvcsOrganizations.isAnySyncDisabled(), is(false));
    }

    @Test
    public void isAllSyncDisabled_returnsFalse_whenBitbucketSyncEnabled() throws Exception {
        setSystemSyncDisabled(false, true, true);
        assertThat(configureDvcsOrganizations.isAllSyncDisabled(), is(false));
    }

    @Test
    public void isAllSyncDisabled_returnsFalse_whenGithubSyncEnabled() throws Exception {
        setSystemSyncDisabled(true, false, true);
        assertThat(configureDvcsOrganizations.isAllSyncDisabled(), is(false));
    }

    @Test
    public void isAllSyncDisabled_returnsFalse_whenGithubEnterpriseSyncEnabled() throws Exception {
        setSystemSyncDisabled(true, true, false);
        assertThat(configureDvcsOrganizations.isAllSyncDisabled(), is(false));
    }

    @Test
    public void isAllSyncDisabled_returnsTrue_whenAllSyncDisabled() throws Exception {
        setSystemSyncDisabled(true, true, true);
        assertThat(configureDvcsOrganizations.isAllSyncDisabled(), is(true));
    }

    @Test
    public void getSyncDisabledWarningMessage_returnsAllMessage_whenGlobalSyncDisabled() throws Exception {
        setSystemSyncDisabled(true, false, false);
        setGlobalSyncDisabled(true);

        final String message = configureDvcsOrganizations.getSyncDisabledWarningMessage();

        assertThat(message, is(SYNCHRONIZATION_ALL_DISABLED_MESSAGE_KEY));
    }

    @Test
    public void getSyncDisabledWarningMessage_returnsSystemMessage_whenGlobalSyncEnabled() throws Exception {
        setSystemSyncDisabled(true, false, false);
        setGlobalSyncDisabled(false);

        final String message = configureDvcsOrganizations.getSyncDisabledWarningMessage();

        assertThat(message, is(i18nMessage(SYNCHRONIZATION_DISABLED_MESSAGE_KEY, BITBUCKET_CLOUD_NAME)));
    }

    @Test
    public void getSyncDisabledWarningMessage_concatenatesSystemNames_whenMultipleSystemSyncDisabled() throws Exception {
        setSystemSyncDisabled(true, true, true);
        setGlobalSyncDisabled(false);

        final String message = configureDvcsOrganizations.getSyncDisabledWarningMessage();

        assertThat(message, is(i18nMessage(SYNCHRONIZATION_DISABLED_MESSAGE_KEY,
                BITBUCKET_CLOUD_NAME + "/" + GIT_HUB_NAME + "/" + GIT_HUB_ENTERPRISE_NAME)));
    }

    @Test
    public void getSyncDisabledWarningTitle_returnsAllTitle_whenGlobalSyncDisabled() throws Exception {
        setSystemSyncDisabled(true, false, false);
        setGlobalSyncDisabled(true);

        final String message = configureDvcsOrganizations.getSyncDisabledWarningTitle();

        assertThat(message, is(ConfigureDvcsOrganizations.SYNCHRONIZATION_ALL_DISABLED_TITLE_KEY));
    }

    @Test
    public void getSyncDisabledWarningTitle_returnsSystemMessage_whenGlobalSyncEnabled() throws Exception {
        setSystemSyncDisabled(true, false, false);
        setGlobalSyncDisabled(false);

        final String message = configureDvcsOrganizations.getSyncDisabledWarningTitle();

        assertThat(message, is(i18nMessage(SYNCHRONIZATION_DISABLED_TITLE_KEY, BITBUCKET_CLOUD_NAME)));
    }

    @Test
    public void getSyncDisabledWarningTitle_concatenatesSystemNames_whenMultipleSystemSyncDisabled() throws Exception {
        setSystemSyncDisabled(true, true, true);
        setGlobalSyncDisabled(false);

        final String message = configureDvcsOrganizations.getSyncDisabledWarningTitle();

        assertThat(message, is(i18nMessage(SYNCHRONIZATION_DISABLED_TITLE_KEY,
                BITBUCKET_CLOUD_NAME + "/" + GIT_HUB_NAME + "/" + GIT_HUB_ENTERPRISE_NAME)));
    }

    @Test
    public void isSyncDisabled_returnsTrue_whenDvcsTypeDisabled() throws Exception {
        setSystemSyncDisabled(true, true, true);

        assertThat(configureDvcsOrganizations.isSyncDisabled(BitbucketCommunicator.BITBUCKET), is(true));
        assertThat(configureDvcsOrganizations.isSyncDisabled(GithubCommunicator.GITHUB), is(true));
        assertThat(configureDvcsOrganizations.isSyncDisabled(GithubEnterpriseCommunicator.GITHUB_ENTERPRISE), is(true));
    }

    @Test
    public void isSyncDisabled_returnsFalse_whenDvcsTypeEnabled() throws Exception {
        setSystemSyncDisabled(false, false, false);

        assertThat(configureDvcsOrganizations.isSyncDisabled(BitbucketCommunicator.BITBUCKET), is(false));
        assertThat(configureDvcsOrganizations.isSyncDisabled(GithubCommunicator.GITHUB), is(false));
        assertThat(configureDvcsOrganizations.isSyncDisabled(GithubEnterpriseCommunicator.GITHUB_ENTERPRISE), is(false));
    }

    @Test
    public void isSyncDisabled_returnsFalse_whenUnknownDvcsType() throws Exception {
        setSystemSyncDisabled(true, true, true);

        assertThat(configureDvcsOrganizations.isSyncDisabled("This is not a type!"), is(false));
    }

    @Test
    public void showFeatureDiscovery_returnsTrue_whenUserHasNotAlreadySeenIt() {
        when(featureDiscoveryService.hasUserSeenFeatureDiscovery()).thenReturn(false);
        assertThat(configureDvcsOrganizations.shouldShowFeatureDiscoveryForUser(), is(true));
    }

    @Test
    public void showFeatureDiscovery_returnsFalse_whenUserHasAlreadySeenIt() {
        when(featureDiscoveryService.hasUserSeenFeatureDiscovery()).thenReturn(true);
        assertThat(configureDvcsOrganizations.shouldShowFeatureDiscoveryForUser(), is(false));
    }

    private Organization createOrganization(final String name, boolean integratedAccount) {
        final Organization result = new Organization();
        result.setName(name);
        result.setCredential(integratedAccount ?
                CredentialFactory.create2LOCredential("key", "secret") :
                CredentialFactory.createPrincipalCredential("Principal"));
        return result;
    }

    private void setSystemSyncDisabled(final boolean bitbucket, final boolean github, final boolean githubEnterprise) {
        when(syncDisabledHelper.isBitbucketSyncDisabled()).thenReturn(bitbucket);
        when(syncDisabledHelper.isGithubSyncDisabled()).thenReturn(github);
        when(syncDisabledHelper.isGithubEnterpriseSyncDisabled()).thenReturn(githubEnterprise);
    }

    private void setGlobalSyncDisabled(final boolean disabled) {
        when(syncDisabledHelper.isSyncDisabled()).thenReturn(disabled);
    }
}