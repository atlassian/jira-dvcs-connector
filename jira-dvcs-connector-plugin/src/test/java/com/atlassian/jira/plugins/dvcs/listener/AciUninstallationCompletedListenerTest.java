package com.atlassian.jira.plugins.dvcs.listener;

import com.atlassian.fusion.aci.api.event.UninstallationCompletedEvent;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.credential.Credential;
import com.atlassian.jira.plugins.dvcs.model.credential.CredentialFactory;
import com.atlassian.jira.plugins.dvcs.spi.bitbucket.clientlibrary.model.BitbucketConstants;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AciUninstallationCompletedListenerTest extends AbstractLifeCycleListenerTest {
    @Test
    public void testWithInstallationWithUnknownPrincipal() {
        // setup
        UninstallationCompletedEvent event = mockEventWithUnknownPrincipal();

        // execute
        aciLifeCycleListener.removeDvcsOrganisation(event);

        // check
        assertOrgRemovalAndAnalyticsDoNotOccur();
    }

    @Test
    public void testWithInstallationWithUnknownBaseUrl() {
        // setup
        UninstallationCompletedEvent event = mockEventWithUnknownBaseUrl();

        // execute
        aciLifeCycleListener.removeDvcsOrganisation(event);

        // check
        assertOrgRemovalAndAnalyticsDoNotOccur();
    }

    @Test
    public void testWithEventForOrganizationNotManagedByAci() {
        // setup
        UninstallationCompletedEvent event = mockEventForOrgNotManagedByAci();

        // execute
        aciLifeCycleListener.removeDvcsOrganisation(event);

        // check
        assertOrgRemovalAndAnalyticsDoNotOccur();
    }

    @Test
    public void testWithEventForOrganizationManagedByAci() {
        // setup
        UninstallationCompletedEvent event = mockEventWithDefaults();

        // execute
        aciLifeCycleListener.removeDvcsOrganisation(event);

        // check
        assertOrgRemovalDoesOccur();
    }

    @Override
    protected void executeListenerWithEventWithWrongApplicationId() {
        UninstallationCompletedEvent event = mockEventWithDefaults();
        when(event.getApplicationId()).thenReturn("some unknown application id");
        aciLifeCycleListener.removeDvcsOrganisation(event);
    }

    @Override
    protected void executeListenerWithNullEvent() {
        aciLifeCycleListener.removeDvcsOrganisation(null);
    }

    private UninstallationCompletedEvent mockEventWithUnknownPrincipal() {
        UninstallationCompletedEvent event = mockEventWithDefaults();
        when(installPayload.getClientKey()).thenReturn("unknown client key");
        when(installPayload.getPrincipalUsername()).thenReturn("unknown username");
        when(installPayload.getPrincipalUuid()).thenReturn("unknown uuid");
        return event;
    }

    private UninstallationCompletedEvent mockEventWithUnknownBaseUrl() {
        UninstallationCompletedEvent event = mockEventWithDefaults();
        when(installPayload.getBaseUrl()).thenReturn("unknown base url");
        return event;
    }

    private UninstallationCompletedEvent mockEventForOrgNotManagedByAci() {
        final Credential nonAciCredential = CredentialFactory.createUnauthenticatedCredential();
        final Organization nonAciOrg = new Organization(ORG_ID, BASE_URL, "org", "bitbucket",
                false, nonAciCredential, null, false, null);
        when(organizationService.getByHostAndName(BASE_URL, USERNAME)).thenReturn(nonAciOrg);
        return mockEventWithDefaults();
    }

    private UninstallationCompletedEvent mockEventWithDefaults() {
        UninstallationCompletedEvent event = mock(UninstallationCompletedEvent.class);
        when(event.getApplicationId()).thenReturn(BitbucketConstants.BITBUCKET_CONNECTOR_APPLICATION_ID);
        when(event.getPrincipalId()).thenReturn(PRINCIPAL_UUID);
        when(event.getConnectApplication()).thenReturn(connectApplication);
        when(event.getInstallation()).thenReturn(installation);
        return event;
    }

    private void assertOrgRemovalAndAnalyticsDoNotOccur() {
        verify(organizationService, never()).remove(anyInt());
        verify(analyticsService, never()).publishConnectOrganisationRemoved(any(Organization.class));
    }

    private void assertOrgRemovalDoesOccur() {
        verify(organizationService, times(1)).remove(ORG_ID);
    }
}