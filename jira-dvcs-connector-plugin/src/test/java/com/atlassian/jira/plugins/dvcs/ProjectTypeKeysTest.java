package com.atlassian.jira.plugins.dvcs;

import com.atlassian.jira.project.type.ProjectTypeKey;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

/**
 * Validates our assumptions about the equality of {@link ProjectTypeKey}s.
 */
public class ProjectTypeKeysTest {
    @Test
    public void keyObjectsShouldBeEqualForTheSameKeyLiteral() {
        final String key = "foo";
        assertThat(new ProjectTypeKey(key), equalTo(new ProjectTypeKey(key)));
    }

    @Test
    public void keyObjectsShouldNotBeEqualForDifferentKeyLiterals() {
        assertThat(new ProjectTypeKey("foo"), not(equalTo(new ProjectTypeKey("bar"))));
    }
}
